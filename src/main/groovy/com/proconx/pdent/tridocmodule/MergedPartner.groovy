package com.proconx.pdent.tridocmodule

/**
 * Created by peto.robert on 2016.01.15..
 */
class MergedPartner extends Partner {
    Partner triDokPartner
    Partner pdePartner

    public MergedPartner(Partner pdePartner, Partner triDokPartner) {
        this.pdePartner = pdePartner
        this.triDokPartner = triDokPartner

//        if (triDokPartner == null) {
//            this.mergeFromPde()
//        } else if (pdePartner == null) {
//            this.mergeFromTriDok()
//        } else {
//            this.merge()
//        }

        this.mergeFromTriDok()

    }

    public void mergeFromTriDok() {
        this.tridokId = triDokPartner.tridokId
        this.pdeId = pdePartner.pdeId
        this.custid = pdePartner.custid
        this.name = triDokPartner.name
        this.compid = triDokPartner.compid
        this.vatNumber = triDokPartner.vatNumber
        this.accountNumber1 = triDokPartner.accountNumber1
        this.accountNumber2 = triDokPartner.accountNumber2
        this.accountNumber3 = triDokPartner.accountNumber3
        this.mkik = triDokPartner.mkik
        this.mkkir = triDokPartner.mkkir
        this.city = triDokPartner.city
        this.address1 = triDokPartner.address1
        this.address2 = triDokPartner.address2
        this.postalNumber = triDokPartner.postalNumber
        this.country = triDokPartner.country
        this.contractPerson = triDokPartner.contractPerson
        this.email = triDokPartner.email
        this.phone = triDokPartner.phone
        this.fax = triDokPartner.fax
        this.mobile = triDokPartner.mobile
        this.web = triDokPartner.web
        this.extId = triDokPartner.tridokId
    }

    public void mergeFromPde() {
        this.tridokId = pdePartner.tridokId
        this.pdeId = pdePartner.pdeId
        this.custid = pdePartner.custid
        this.name = pdePartner.name
        this.compid = pdePartner.compid
        this.vatNumber = pdePartner.vatNumber
        this.accountNumber1 = pdePartner.accountNumber1
        this.accountNumber2 = pdePartner.accountNumber2
        this.accountNumber3 = pdePartner.accountNumber3
        this.mkik = pdePartner.mkik
        this.mkkir = pdePartner.mkkir
        this.city = pdePartner.city
        this.address1 = pdePartner.address1
        this.address2 = pdePartner.address2
        this.postalNumber = pdePartner.postalNumber
        this.country = pdePartner.country
        this.contractPerson = pdePartner.contractPerson
        this.email = pdePartner.email
        this.phone = pdePartner.phone
        this.fax = pdePartner.fax
        this.mobile = pdePartner.mobile
        this.web = pdePartner.web
        this.extId = pdePartner.extId
    }

    private boolean isEmpty(String str) {
        return str == null || str.equals("");
    }

    public boolean isAddressOK(Partner pdePartner) {
        boolean cityOk = !isEmpty(pdePartner.city)
        boolean addressOk = !isEmpty(pdePartner.address1)
        boolean zipOk = !isEmpty(pdePartner.postalNumber)
        return cityOk && addressOk && zipOk
    }

    public void merge() {
        this.tridokId = triDokPartner.tridokId
        this.pdeId = pdePartner.pdeId
        this.custid = pdePartner.custid
        this.name = triDokPartner.name
        this.compid = isEmpty(pdePartner.compid) ? triDokPartner.compid : pdePartner.compid
        this.vatNumber = isEmpty(pdePartner.vatNumber) ? triDokPartner.vatNumber : pdePartner.vatNumber
        this.accountNumber1 = isEmpty(pdePartner.accountNumber1) ? triDokPartner.accountNumber1 : pdePartner.accountNumber1
        this.accountNumber2 = isEmpty(pdePartner.accountNumber2) ? triDokPartner.accountNumber2 : pdePartner.accountNumber2
        this.accountNumber3 = isEmpty(pdePartner.accountNumber3) ? triDokPartner.accountNumber3 : pdePartner.accountNumber3
        this.mkik = isEmpty(pdePartner.mkik) ? triDokPartner.mkik : pdePartner.mkik
        this.mkkir = isEmpty(pdePartner.vatNumber) ? triDokPartner.mkkir : pdePartner.mkkir

        boolean addressOk = isAddressOK(pdePartner)
        this.city = addressOk ? pdePartner.city : triDokPartner.city

        if (addressOk) {
            if (isEmpty(pdePartner.address2)) {
                this.address1 = pdePartner.address1
                this.address2 = pdePartner.address2
            } else {
                this.address1 = "${pdePartner.address1} ${pdePartner.address2}"
            }
        } else {
            this.address1 = triDokPartner.address1
            this.address2 = triDokPartner.address2
        }
        this.postalNumber = addressOk ? pdePartner.postalNumber : triDokPartner.postalNumber

        this.contractPerson = isEmpty(pdePartner.contractPerson) ? triDokPartner.contractPerson : pdePartner.contractPerson
        this.email = isEmpty(pdePartner.email) ? triDokPartner.email : pdePartner.email
        this.phone = isEmpty(pdePartner.phone) ? triDokPartner.phone : pdePartner.phone
        this.fax = isEmpty(pdePartner.fax) ? triDokPartner.fax : pdePartner.fax
        this.mobile = isEmpty(pdePartner.mobile) ? triDokPartner.mobile : pdePartner.mobile
        this.web = isEmpty(pdePartner.web) ? triDokPartner.web : pdePartner.web
    }

    public String triplet(String prop) {
        return "<font style='color:green'><b>${this."$prop"}</b></font><br>T: ${this.triDokPartner."$prop"}<br>P: ${this.pdePartner."$prop"}"
    }

}
