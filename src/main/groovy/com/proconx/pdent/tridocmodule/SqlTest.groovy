package com.proconx.pdent.tridocmodule

import groovy.sql.Sql

/**
 * Created by peto.robert on 2016.09.28..
 */

def url = "jdbc:oracle:thin:@192.168.1.12:1521:pdeprod"
def user = "pdeaqua"
def password = "pdeaqua246"
def driver = "oracle.jdbc.driver.OracleDriver"


100.times {
    Sql sql = null
    try {
        println "Creating sql $it times"
        sql = Sql.newInstance(url, user, password, driver)
        sql.withStatement {
            stmt -> stmt.queryTimeout = 100
        }

        sql.execute("select count(*) from contracts")
    } finally {
        if (sql != null) {
            sql.close()
        }
    }
}
