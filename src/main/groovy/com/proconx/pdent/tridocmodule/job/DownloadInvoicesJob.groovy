package com.proconx.pdent.tridocmodule.job

import com.proconx.pdent.tridocmodule.Invoice
import com.proconx.pdent.tridocmodule.db.scripts.DownloadInvoiceScript
import com.proconx.pdent.tridocmodule.db.scripts.InsertInvoiceScript
import com.proconx.pdent.tridocmodule.html.HtmlTableView
import com.proconx.pdent.tridocmodule.html.InvoiceHtmlTableView
import com.proconx.pdent.tridocmodule.wsclient.TriDokInvoiceParser
import com.proconx.pdent.tridocmodule.wsclient.WsDownloader
import com.proconx.pdent.tridocmodule.wsclient.connector.RestConnector
import com.proconx.pdent.tridocmodule.wsclient.merge.MergeInvoiceList
import groovyx.net.http.ContentType
import groovyx.net.http.Method

/**
 * Created by peto.robert on 2015.12.15..
 */


class DownloadInvoicesJob {

    def live = false

    InvoiceType invoiceType

    def merge = new MergeInvoiceList()


    public DownloadInvoicesJob(InvoiceType invoiceType) {
        this.invoiceType = invoiceType
    }


    void downloadInvoices() {
        def wsDownloader = new WsDownloader()
        def connector = new RestConnector("http://192.168.1.13:8080/TriDoc/login")
        connector.httpBuilder.request(Method.POST, ContentType.XML) {
            body = wsDownloader.authenticate()
        }

        connector.httpBuilder.request(Method.POST, ContentType.TEXT) {
            uri.path = "http://192.168.1.13:8080/TriDoc/report/tridoc/docList"
            body = downloadInvoiceScript()
            response.success = { resp, xml ->
                TriDokInvoiceParser parser = TriDokInvoiceParser.buildDefaultParser()
                List<Invoice> tridokInvoices = parser.parseInvoices(xml)
                List<Invoice> pdeInvoices = new DownloadInvoiceScript().downloadInvoiceChunks()
                merge.merge(pdeInvoices, tridokInvoices)
                merge.addToPde.each { Invoice invoice ->
                    // println "Adding invoice to PDE: $invoice"

                    //   println "INV: ${invoice.custid} ->${invoice.mayImportable()}"
                    if (invoice.mayImportable() && live) {
                        new InsertInvoiceScript().insertInvoice(invoice)
                    }
                }



                HtmlTableView tableView = new InvoiceHtmlTableView()
                tableView.invoices = merge.addToPde
                def html = tableView.html()

                File f = new File("C:/pdent", "report_invoice_${this.invoiceType.name}.html")
                f.write(html)
            }
            headers = ["Accept": "application/text", "Content-Type": "application/text; charset=utf-8", "Cookie": connector.cookies.join(';')]
        }
    }


    public static DownloadInvoicesJob buildJob(InvoiceType type) {
        return new DownloadInvoicesJob(type)
    }


    public String downloadInvoiceScript() {
        """
<request>
  <action>show</action>
  <entity>
\t<tridoc:docListParam xmlns:tridoc="http://www.trilobita.hu/schema/tridoc/model-1_0_0">
      <tridoc:docType><tridoc:id>${this.invoiceType.code}</tridoc:id></tridoc:docType>
\t      <tridoc:docExpired>false</tridoc:docExpired>
\t\t\t<tridoc:showCustomFields>true</tridoc:showCustomFields>
 \t</tridoc:docListParam>
  </entity>
</request>
"""

    }

}
//@CompileStatic
//enum InvoiceType {
//
//}