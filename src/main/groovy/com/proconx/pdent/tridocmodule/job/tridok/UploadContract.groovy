package com.proconx.pdent.tridocmodule.job.tridok

import com.proconx.pdent.tridocmodule.Contract
import com.proconx.pdent.tridocmodule.job.full.FullData
import com.proconx.pdent.tridocmodule.wsclient.TriDokContractParser
import com.proconx.pdent.tridocmodule.wsclient.WsDownloader
import com.proconx.pdent.tridocmodule.wsclient.connector.RestConnector
import groovyx.net.http.ContentType
import groovyx.net.http.Method

/**
 * Created by peto.robert on 2016.01.29..
 */
class UploadContract {

    def wsDownloader = new WsDownloader()
    def connector = new RestConnector("http://192.168.1.13:8080/TriDoc/login")

    void auth() {
        connector.httpBuilder.request(Method.POST, ContentType.XML) {
            body = wsDownloader.authenticate()
        }
    }

    void uploadContract(Contract contract) {
        TriDokContractParser parser = TriDokContractParser.buildDefaultParser()
        parser.partners = FullData.db().downloadPartners.tridokPartners
        String conrtactXml = parser.generate(contract)
        println "$conrtactXml"
        connector.httpBuilder.request(Method.POST, ContentType.TEXT) {
            uri.path = "http://192.168.1.13:8080/TriDoc/entity/tridoc/doc"
            body = conrtactXml
        }
    }
}
