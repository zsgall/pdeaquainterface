package com.proconx.pdent.tridocmodule.job

import com.proconx.pdent.tridocmodule.Partner
import com.proconx.pdent.tridocmodule.db.scripts.DownloadPartnerScript
import com.proconx.pdent.tridocmodule.wsclient.TriDokPartnerParser
import com.proconx.pdent.tridocmodule.wsclient.WsDownloader
import com.proconx.pdent.tridocmodule.wsclient.connector.RestConnector
import com.proconx.pdent.tridocmodule.wsclient.merge.MergePartnerList
import groovyx.net.http.ContentType
import groovyx.net.http.Method

/**
 * Created by peto.robert on 2015.12.13..
 */
class SyncPartners {

    def live = false

    MergePartnerList merge = new MergePartnerList()

    static String letters = "'\"„ASDFGHJKLQWERTYUIOPZXCVBNM"

    List<Partner> allTriDok = new ArrayList<>()
    List<Partner> allPde = new ArrayList<>()
    def wsDownloader = new WsDownloader()
    def connector = new RestConnector("http://192.168.1.13:8080/TriDoc/login")

    void sync() {
        connector.httpBuilder.request(Method.POST, ContentType.XML) {
            body = wsDownloader.authenticate()
        }

        sync(0)
    }

    void syncDone() {
        List<Partner> pdePartners = new DownloadPartnerScript().downloadPartners()
        allPde.addAll(pdePartners)

        println "PDE: ${allPde.size()} TR: ${allTriDok.size()})"

        merge.merge(allPde, allTriDok)
        merge.newInPde.each { Partner partner ->
            TriDokPartnerParser parser = TriDokPartnerParser.buildDefaultParser()
            println "Adding partner to tridok: $partner"
            if (live && partner.name == "AquaTrade and Service Kft") {
                String conrtactXml = parser.generate(partner)
                println "$conrtactXml"
//                        connector.httpBuilder.request(Method.POST, ContentType.TEXT) {
//                            uri.path = "http://192.168.1.13:8080/TriDoc/entity/tripartner/firm"
//                            body = conrtactXml
//                        }
            }
        }

//        merge.tridok.each { Partner partner ->
//            InsertPartnerScript insert = new InsertPartnerScript()
//              println "Adding partner to pde: $partner"
//            if (live) {
//                insert.insertPartner(partner)
//            }
//        }
    }

    void sync(int i) {


        connector.httpBuilder.request(Method.POST, ContentType.TEXT) {
            uri.path = "http://192.168.1.13:8080/TriDoc/report/tripartner/firmList"
            body = """
<request>
  <action>show</action>
  <entity>
    <tridoc:firmListParam xmlns:tridoc="http://www.trilobita.hu/schema/tripartner/model-1_0_0">
                    <tridoc:name><![CDATA[${letters[i]}%]]></tridoc:name>
                    <tridoc:showCustomFields>true</tridoc:showCustomFields>
    </tridoc:firmListParam>
  </entity>
</request>
"""
            response.success = { resp, xml ->
                TriDokPartnerParser parser = TriDokPartnerParser.buildDefaultParser()
                List<Partner> tridokPartners = parser.parsePartners(xml)
                allTriDok.addAll(tridokPartners)
                if (i < letters.length() - 1) {
                    sync(i + 1)
                } else {
                    syncDone()

                }
                headers = ["Accept": "application/text", "Content-Type": "application/text; charset=utf-8", "Cookie": connector.cookies.join(';')]
            }
        }
    }

    static final String DOWNLOAD_PARTNERS = """
<request>
  <action>show</action>
  <entity>
    <tridoc:firmListParam xmlns:tridoc="http://www.trilobita.hu/schema/tripartner/model-1_0_0">
                    <tridoc:showCustomFields>true</tridoc:showCustomFields>
    </tridoc:firmListParam>
  </entity>
</request>
"""
}
