package com.proconx.pdent.tridocmodule.job.tridok

import com.proconx.pdent.tridocmodule.Worknum
import com.proconx.pdent.tridocmodule.wsclient.TriDokWorknumParser
import com.proconx.pdent.tridocmodule.wsclient.WsDownloader
import com.proconx.pdent.tridocmodule.wsclient.connector.RestConnector
import groovyx.net.http.ContentType
import groovyx.net.http.Method

/**
 * Created by peto.robert on 2016.01.22..
 */
class DownloadWorknums {


    final static patterns = ["M-%", "MM-%", "AEM-%"]

    List<Worknum> tridokWorknums = new ArrayList<>()
    def wsDownloader = new WsDownloader()
    def connector = new RestConnector("http://192.168.1.13:8080/TriDoc/login")


    void auth() {
        connector.httpBuilder.request(Method.POST, ContentType.XML) {
            body = wsDownloader.authenticate()
        }
    }


    void downloadWorknums() {
        auth()
        tridokWorknums = new ArrayList<>()
        patterns.each { String p ->
            downloadWorknumsWithPattern(p)
        }
    }


    void downloadWorknumsWithPattern(String pattern) {
        connector.httpBuilder.request(Method.POST, ContentType.TEXT) {
            uri.path = "http://192.168.1.13:8080/TriDoc/lov/tridoc/prjs"
            body = downloadWorknumXml(pattern)
            response.success = { resp, xml ->
                TriDokWorknumParser parser = TriDokWorknumParser.buildDefaultParser()
                List<Worknum> worknums = parser.parseWorkNums(xml)
                println "Worknums ${pattern} (${worknums.size()})"
                if (worknums.size() == 100) {
                    throw new Exception("Possible data lost: Tridok returned 100 rows!!!")
                }

                tridokWorknums.addAll(worknums)
            }
            headers = ["Accept": "application/text", "Content-Type": "application/text; charset=utf-8", "Cookie": connector.cookies.join(';')]
        }
    }

    public String downloadWorknumXml(String pattern) {
        """
                   <request>
            <action>show</action>
            <parameters>
            <name>${pattern}</name>
            </parameters>
        </request>
        """
    }

    public static void main(String[] args) {
        DownloadWorknums downloadWorknumsJob = new DownloadWorknums()
        downloadWorknumsJob.downloadWorknums()
    }
}
