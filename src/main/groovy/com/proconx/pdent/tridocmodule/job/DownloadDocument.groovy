package com.proconx.pdent.tridocmodule.job

import com.proconx.pdent.tridocmodule.wsclient.WsDownloader
import com.proconx.pdent.tridocmodule.wsclient.connector.RestConnector
import groovyx.net.http.ContentType
import groovyx.net.http.Method

/**
 * Created by peto.robert on 2016.01.17..
 */
class DownloadDocument {

    void download(Long docId) {
        def wsDownloader = new WsDownloader()
        def connector = new RestConnector("http://192.168.1.13:8080/TriDoc/login")
        connector.httpBuilder.request(Method.POST, ContentType.XML) {
            body = wsDownloader.authenticate()
        }

        connector.httpBuilder.get(uri: "http://192.168.1.13:8080/TriDoc/file/download?type=file&id=${docId}", contentType: ContentType.BINARY) {

            resp, inputStream ->
                String file = "c:/pdent/temp/pdf_${docId}.pdf"
                FileOutputStream out = new FileOutputStream(file)
                byte[] buffer = new byte[1024];
                int len = inputStream.read(buffer);
                while (len != -1) {
                    out.write(buffer, 0, len);
                    len = inputStream.read(buffer);
                }

                Process p = Runtime.getRuntime().exec("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe ${file}");

        }
    }

    public static void main(String[] args) {
        if (args.length == 1) {
            new DownloadDocument().download(Long.parseLong(args[0]))
        }
    }

}