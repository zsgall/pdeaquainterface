package com.proconx.pdent.tridocmodule.job.full

import com.proconx.pdent.tridocmodule.Invoice
import com.proconx.pdent.tridocmodule.TriDokInvoice
import com.proconx.pdent.tridocmodule.db.scripts.InsertInvoiceScript
import com.proconx.pdent.tridocmodule.html.HtmlTableView
import com.proconx.pdent.tridocmodule.html.InvoiceHtmlTableView
import com.proconx.pdent.tridocmodule.wsclient.merge.MergeInvoiceList

/**
 * Created by peto.robert on 2016.01.22..
 */
class ExecuteInvoiceJob {

    void execute() {
        executeJob(true)
    }

    void executeReport() {
        executeJob(false)
    }

    void executeJob(boolean liveMode) {
        FullData db = FullData.db()
        MergeInvoiceList merge = new MergeInvoiceList()
        merge.merge(db.selectInvoices.pdeInvoices, db.downloadInvoices.tridokInvoices)

        merge.union.each { Invoice triDokInvoice ->
            println "Matches found: ${triDokInvoice.toString()}"
        }

        merge.tridokInvoices.each { TriDokInvoice triDokInvoice ->
            println "Adding to pde: ${triDokInvoice.toString()}"
            if (liveMode && triDokInvoice.mayImportable()) {
                InsertInvoiceScript insert = new InsertInvoiceScript()
                insert.insertInvoice(triDokInvoice)
            }
        }

        generateHtmlReport(merge.tridokInvoices, merge.union)
    }

    public static generateHtmlReport(List<Invoice> newInvoices, List<Invoice> mergeInvoices) {
        List<Invoice> all = new ArrayList<>()
        if (newInvoices != null)
            all.addAll(newInvoices)
        if (mergeInvoices != null)
            all.addAll(mergeInvoices)

        HtmlTableView tableView = new InvoiceHtmlTableView()
        tableView.invoices = all
        def html = tableView.html()

        File f = new File("C:/pdent/newInTridok", "report_invoices.html")
        f.write(html)

    }


    public static void main(String[] args) {
        ExecuteInvoiceJob executeInvoiceJob = new ExecuteInvoiceJob()
        executeInvoiceJob.executeReport()
    }

}
