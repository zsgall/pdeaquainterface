package com.proconx.pdent.tridocmodule.job.tridok

import com.proconx.pdent.tridocmodule.TriDokContract
import com.proconx.pdent.tridocmodule.wsclient.TriDokContractParser
import com.proconx.pdent.tridocmodule.wsclient.WsDownloader
import com.proconx.pdent.tridocmodule.wsclient.connector.RestConnector
import groovyx.net.http.ContentType
import groovyx.net.http.Method

import java.text.SimpleDateFormat

/**
 * Created by peto.robert on 2016.01.21..
 */
class DownloadContracts {


    List<TriDokContract> tridokContracts = new ArrayList<>()
    def wsDownloader = new WsDownloader()
    def connector = new RestConnector("http://192.168.1.13:8080/TriDoc/login")

    void auth() {
        connector.httpBuilder.request(Method.POST, ContentType.XML) {
            body = wsDownloader.authenticate()
        }
    }

    void downloadInterval(Date from, Date till) {
        auth()
        this.tridokContracts = new ArrayList<>()

        ContractType.values().each { ContractType itype ->
            Calendar calendar = Calendar.getInstance(); // this would default to now
            calendar.setTime(from)

            // println "XTYPE: ${itype.category}/${itype.type}"
            while (calendar.getTime() < till) {

                downloadDocumentsAt(itype, calendar.getTime())

                calendar.add(Calendar.DAY_OF_MONTH, 1)
            }
        }

        println "FULL (${tridokContracts.size()})"

    }

    void downloadDocumentsAt(ContractType type, Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd")

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date)
        calendar.add(Calendar.DAY_OF_MONTH, -1)
        String from = "${sdf.format(calendar.getTime())}T23:00:00Z"
        String till = "${sdf.format(date)}T22:59:00Z"

        // println "${downloadContractXml(type, from, till)}"

        connector.httpBuilder.request(Method.POST, ContentType.TEXT) {
            uri.path = "http://192.168.1.13:8080/TriDoc/report/tridoc/docList"
            body = downloadContractXml(type, from, till)
            response.success = { resp, xml ->
                TriDokContractParser parser = TriDokContractParser.buildDefaultParser()
                List<TriDokContract> contracts = parser.parseContractChunks(xml)
                if (contracts.size()) println "Contracts from ${from} till ${till} (${contracts.size()})"
                if (contracts.size() == 100) {
                    throw new Exception("Possible data lost: Tridok returned 100 rows!!!")
                }

                contracts.each { TriDokContract c1 ->
                    if (!tridokContracts.find { TriDokContract c2 -> c2.tridokId == c1.tridokId })
                        tridokContracts.add(c1)
                }
            }
            headers = ["Accept": "application/text", "Content-Type": "application/text; charset=utf-8", "Cookie": connector.cookies.join(';')]
        }
    }

    public String downloadContractXml(ContractType type, String from, String till) {
        //    from = "2016-01-01T00:00:00Z"
        //   till = "2016-02-01T00:00:00Z"
        """
            <request>
              <action>show</action>
              <entity>
                    <tridoc:docListParam xmlns:tridoc="http://www.trilobita.hu/schema/tridoc/model-1_0_0">
                         <tridoc:dateFilterType><![CDATA[interval]]></tridoc:dateFilterType>
                         <tridoc:startDate>${from}</tridoc:startDate>
                         <tridoc:endDate>${till}</tridoc:endDate>
                        <tridoc:docType><tridoc:id>${type.type}</tridoc:id></tridoc:docType>
                        <tridoc:docExpired>false</tridoc:docExpired>
                        <tridoc:showCustomFields>true</tridoc:showCustomFields>
                    </tridoc:docListParam>
              </entity>
            </request>
        """
    }

    public static void main(String[] args) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd")
        Date from = sdf.parse("2016-01-01")
        Date till = sdf.parse("2016-01-23")

        DownloadContracts downloadContracts = new DownloadContracts()
        downloadContracts.downloadInterval(from, till)

        downloadContracts.tridokContracts.each {
            println "C: ${it} ${it.contractType}"
        }
    }
}

enum ContractType {
    AG001("AG", "001", 28, 53),
    MM001("MM", "001", 29, 54),
    AE001("AE", "001", 30, 56),
    AG002("AG", "002", 31, 57),
    MM002("MM", "002", 32, 58),
    AE002("AE", "002", 33, 59),
    AG003("AG", "003", 34, 60),
    MM003("MM", "003", 35, 61),
    AE003("AE", "003", 36, 62),
    AG004("AG", "004", 37, 63),
    MM004("MM", "004", 38, 64),
    AE004("AE", "004", 39, 65),
    AG005("AG", "005", 43, 69),
    MM005("MM", "005", 44, 70),
    AE005("AE", "005", 45, 71),
    AG006("AG", "006", 40, 66),
    MM006("MM", "006", 41, 67),
    AE006("AE", "006", 42, 68),
    AG007("AG", "007", 22, 141),
    MM007("MM", "007", 23, 142),
    AE007("AE", "007", 24, 143),
    AG008("AG", "008", 19, 138),
    MM008("MM", "008", 20, 139),
    AE008("AE", "008", 21, 140)

    String company
    String code
    Long category
    Long type

    ContractType(String company, String code, Long category, Long type) {
        this.company = company
        this.code = code
        this.category = category
        this.type = type
    }

    static ContractType getBy(String company, String code) {
        println "TYPE Get by: '${company}'/'${code}' (C)"
        ContractType type = null
        values().each { ContractType ctype ->
            if (ctype.code == code && ctype.company == company) {
                type = ctype
            }
        }
        return type
    }

    static ContractType getBy(Long category, Long type) {
        // println "Get by: ${category}/${type} (C)"
        ContractType ret = null
        values().each { ContractType ctype ->
            if (ctype.category == category && ctype.type == type) {
                ret = ctype
            }
        }
        return ret
    }
}