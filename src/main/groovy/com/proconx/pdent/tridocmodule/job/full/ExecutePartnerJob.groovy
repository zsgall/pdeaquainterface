package com.proconx.pdent.tridocmodule.job.full

import com.proconx.pdent.tridocmodule.Partner
import com.proconx.pdent.tridocmodule.db.scripts.InsertPartnerScript
import com.proconx.pdent.tridocmodule.html.HtmlTableView
import com.proconx.pdent.tridocmodule.html.PartnerHtmlTableView
import com.proconx.pdent.tridocmodule.wsclient.merge.MergePartnerList

/**
 * Created by peto.robert on 2016.01.22..
 */
class ExecutePartnerJob {

    void execute() {
        executeJob(true)
    }

    void executeReport() {
        executeJob(false)
    }

    void executeJob(boolean liveMode) {
        FullData db = FullData.db()
        MergePartnerList merge = new MergePartnerList()
        merge.merge(db.selectPartners.pdePartners, db.downloadPartners.tridokPartners)
        merge.newInTridok.each { Partner triDokPartner ->
//                println "Adding to pde: ${triDokPartner.name}"
            if (liveMode) {
                //Insert to PDE
                InsertPartnerScript insertPartnerScript = new InsertPartnerScript()
                insertPartnerScript.insertPartner(triDokPartner)
            } else {
                println "Adding to pde: ${triDokPartner.name}"
            }
        }
//
//        UploadPartner uploadPartner =new UploadPartner()
//        uploadPartner.auth()
//
//        merge.pde.each { Partner pdePartner ->
//            if ( liveMode) {
//                uploadPartner.uploadPartner(pdePartner)
//
//            } else {
//                println "Adding to tridok: ${pdePartner}"
//            }
//
//        }
//
//        merge.union.each {MergedPartner mergedPartner ->
//            println "Merging ${mergedPartner.toString()}"
//        }
        generateHtmlReport(merge.newInTridok, merge.newInPde, merge.union)
    }

    public static generateHtmlReport(List<Partner> addToPde, List<Partner> addToTriDok, List<Partner> mergePartners) {
        List<Partner> all = new ArrayList<>()
        if (addToPde != null)
            all.addAll(addToPde)
        if (addToTriDok != null)
            all.addAll(addToTriDok)

        if (mergePartners != null)
            all.addAll(mergePartners)

        HtmlTableView tableView = new PartnerHtmlTableView()
        tableView.partners = all
        def html = tableView.html()

        File f = new File("C:/pdent/newInTridok", "report_partners.html")
        f.write(html)

    }

    public static void main(String[] args) {
        ExecutePartnerJob executePartnerJob = new ExecutePartnerJob()
        executePartnerJob.executeReport()
    }

}
