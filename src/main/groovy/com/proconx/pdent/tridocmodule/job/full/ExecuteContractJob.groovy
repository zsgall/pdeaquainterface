package com.proconx.pdent.tridocmodule.job.full

import com.proconx.pdent.tridocmodule.Contract
import com.proconx.pdent.tridocmodule.TriDokContract
import com.proconx.pdent.tridocmodule.html.ContractHtmlTableView
import com.proconx.pdent.tridocmodule.html.HtmlTableView
import com.proconx.pdent.tridocmodule.job.tridok.UploadContract
import com.proconx.pdent.tridocmodule.wsclient.TriDokContractParser
import com.proconx.pdent.tridocmodule.wsclient.merge.MergeContractList

/**
 * Created by peto.robert on 2016.01.25..
 */
class ExecuteContractJob {

    void execute() {
        executeJob(true)
    }

    void executeReport() {
        executeJob(false)
    }

    void executeJob(boolean liveMode) {
        FullData db = FullData.db()
        MergeContractList merge = new MergeContractList()
        merge.merge(db.selectContracts.pdeContracts, db.downloadContracts.tridokContracts)

        merge.union.each { TriDokContract triDokContract ->
            println "Matches found: ${triDokContract.toString()}"
        }


        TriDokContractParser parser = TriDokContractParser.buildDefaultParser()
        parser.partners = FullData.db().downloadPartners.tridokPartners

        merge.union.each { TriDokContract contract ->
            if (liveMode) {
                if (contract.mayImportable()) {
                    UploadContract uploadContract = new UploadContract()
                    uploadContract.auth()
                    uploadContract.uploadContract(contract)
                }

            } else {
                println "Merging to newInTridok: ${contract.toString()}"
            }

        }

        merge.pde.each { Contract contract ->
            if (liveMode) {
                if (contract.mayImportable()) {
                    UploadContract uploadContract = new UploadContract()
                    uploadContract.auth()
                    uploadContract.uploadContract(contract)
                }

            } else {
                println "Adding to newInTridok: ${contract.toString()}"
            }

        }

        generateHtmlReport(merge.pde, merge.union)
    }

    public static generateHtmlReport(List<Contract> newContracts, List<Contract> mergeContracts) {
        List<Contract> all = new ArrayList<>()
        if (newContracts != null)
            all.addAll(newContracts)
        if (mergeContracts != null)
            all.addAll(mergeContracts)

        HtmlTableView tableView = new ContractHtmlTableView()
        tableView.contracts = all
        def html = tableView.html()

        File f = new File("C:/pdent/newInTridok", "report_contracts.html")
        f.write(html)

    }


    public static void main(String[] args) {
        ExecuteContractJob executeContractJob = new ExecuteContractJob()
        executeContractJob.executeReport()
    }
}
