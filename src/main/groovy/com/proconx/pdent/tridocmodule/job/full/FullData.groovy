package com.proconx.pdent.tridocmodule.job.full

import com.proconx.pdent.tridocmodule.job.pde.SelectContracts
import com.proconx.pdent.tridocmodule.job.pde.SelectInvoices
import com.proconx.pdent.tridocmodule.job.pde.SelectPartners
import com.proconx.pdent.tridocmodule.job.tridok.DownloadContracts
import com.proconx.pdent.tridocmodule.job.tridok.DownloadInvoices
import com.proconx.pdent.tridocmodule.job.tridok.DownloadPartners
import com.proconx.pdent.tridocmodule.job.tridok.DownloadWorknums

import java.text.SimpleDateFormat

/**
 * Created by peto.robert on 2016.01.22..
 */
class FullData {

    static FullData db

    SelectPartners selectPartners = new SelectPartners()
    SelectInvoices selectInvoices = new SelectInvoices()
    SelectContracts selectContracts = new SelectContracts()

    DownloadPartners downloadPartners = new DownloadPartners()
    DownloadWorknums downloadWorknums = new DownloadWorknums()
    DownloadContracts downloadContracts = new DownloadContracts()
    DownloadInvoices downloadInvoices = new DownloadInvoices()

    void selectAll() {
        selectPartners.selectPartners()
        selectInvoices.selectInvoiceChunks()
//        selectContracts.selectContracts()
    }

    void downloadAll() {
        downloadPartners.downloadPartners()
        downloadWorknums.downloadWorknums()

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd")
        Date from = sdf.parse("2017-06-01")
//        Date from = sdf.parse("2018-06-01")
        Date till = sdf.parse(sdf.format(new Date()))
        println "FROM: ${from} TILL: ${till}"
        downloadInvoices.downloadInterval(from, till)
//        downloadContracts.downsloadInterval(from, till)
    }

    void gatherAllData() {
        selectAll()
        downloadAll()
    }

    static execute() {
        if (db == null) {
            db = new FullData()
        }
        db.gatherAllData()
    }

    static FullData db() {
        if (db == null) {
            execute()
        }
        return db
    }

    public static void main(String[] args) {
        FullData data = db()

    }

}
