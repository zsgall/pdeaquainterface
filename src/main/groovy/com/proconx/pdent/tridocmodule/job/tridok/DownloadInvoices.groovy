package com.proconx.pdent.tridocmodule.job.tridok

import com.proconx.pdent.tridocmodule.TriDokInvoice
import com.proconx.pdent.tridocmodule.job.InvoiceType
import com.proconx.pdent.tridocmodule.wsclient.TriDokInvoiceParser
import com.proconx.pdent.tridocmodule.wsclient.WsDownloader
import com.proconx.pdent.tridocmodule.wsclient.connector.RestConnector
import groovyx.net.http.ContentType
import groovyx.net.http.Method

import java.text.SimpleDateFormat

/**
 * Created by peto.robert on 2016.01.21..
 */
class DownloadInvoices {


    List<TriDokInvoice> tridokInvoices = new ArrayList<>()
    def wsDownloader = new WsDownloader()
    def connector = new RestConnector("http://192.168.1.13:8080/TriDoc/login")


    void auth() {
        connector.httpBuilder.request(Method.POST, ContentType.XML) {
            body = wsDownloader.authenticate()
        }
    }


    void downloadInterval(Date from, Date till) {
        auth()
        this.tridokInvoices = new ArrayList<>()

        Calendar calendar = Calendar.getInstance(); // this would default to now
        calendar.setTime(from)


        while (calendar.getTime() < till) {

            InvoiceType.values().each { InvoiceType itype ->

                downloadInvoicesAt(itype, calendar.getTime())
            }
            calendar.add(Calendar.DAY_OF_MONTH, 1)
        }

        println "FULL (${tridokInvoices.size()})"
//        tridokInvoices.each { TriDokInvoice iii ->
//            println "WWWWWW: ${iii.toString()}"
//        }

    }

    void downloadInvoicesAt(InvoiceType type, Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd")

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date)
        calendar.add(Calendar.DAY_OF_MONTH, -1)
        String from = "${sdf.format(calendar.getTime())}T23:00:00Z"
        String till = "${sdf.format(date)}T22:59:00Z"

        connector.httpBuilder.request(Method.POST, ContentType.TEXT) {
            uri.path = "http://192.168.1.13:8080/TriDoc/report/tridoc/docList"
            body = downloadInvoiceXml(type, from, till)
            response.success = { resp, xml ->
                TriDokInvoiceParser parser = TriDokInvoiceParser.buildDefaultParser()
                List<TriDokInvoice> invoices = parser.parseInvoices(xml)
                if (invoices.size()) println "Invoices ${type.code} from ${from} till ${till} (${invoices.size()})"
                if (invoices.size() == 100) {
                    throw new Exception("Possible data lost: Tridok returned 100 rows!!!")
                }

                //   tridokInvoices.addAll(invoices)

                invoices.each { TriDokInvoice c1 ->
                    if (!tridokInvoices.find { TriDokInvoice c2 -> c2.custid == c1.custid })
                        tridokInvoices.add(c1)
                }

            }
            headers = ["Accept": "application/text", "Content-Type": "application/text; charset=utf-8", "Cookie": connector.cookies.join(';')]
        }
    }

    public String downloadInvoiceXml(InvoiceType type, String from, String till) {
        """
            <request>
              <action>show</action>
              <entity>
            <tridoc:docListParam xmlns:tridoc="http://www.trilobita.hu/schema/tridoc/model-1_0_0">
             <tridoc:dateFilterType><![CDATA[interval]]></tridoc:dateFilterType>
             <tridoc:startDate>${from}</tridoc:startDate>
             <tridoc:endDate>${till}</tridoc:endDate>
                  <tridoc:docType><tridoc:id>${type.code}</tridoc:id></tridoc:docType>
                  <tridoc:docExpired>false</tridoc:docExpired>
            <tridoc:showCustomFields>true</tridoc:showCustomFields>


             </tridoc:docListParam>
              </entity>
            </request>
        """
    }

    public static void main(String[] args) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd")
        Date from = sdf.parse("2016-07-30")
        Date till = sdf.parse("2016-09-01")

        DownloadInvoices downloadInvoices = new DownloadInvoices()
        downloadInvoices.downloadInterval(from, till)
    }
}
