package com.proconx.pdent.tridocmodule.job.pde

import com.proconx.pdent.tridocmodule.Partner
import com.proconx.pdent.tridocmodule.db.scripts.DownloadPartnerScript

/**
 * Created by peto.robert on 2016.01.22..
 */
class SelectPartners {

    List<Partner> pdePartners

    void selectPartners() {
        pdePartners = new DownloadPartnerScript().downloadPartners()
    }
}
