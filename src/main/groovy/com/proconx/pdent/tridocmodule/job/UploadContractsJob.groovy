package com.proconx.pdent.tridocmodule.job

import com.proconx.pdent.tridocmodule.Contract
import com.proconx.pdent.tridocmodule.Partner
import com.proconx.pdent.tridocmodule.db.scripts.DownloadContractScript
import com.proconx.pdent.tridocmodule.wsclient.TriDokContractParser
import com.proconx.pdent.tridocmodule.wsclient.TriDokPartnerParser
import com.proconx.pdent.tridocmodule.wsclient.WsDownloader
import com.proconx.pdent.tridocmodule.wsclient.connector.RestConnector
import com.proconx.pdent.tridocmodule.wsclient.merge.MergeContractList
import groovyx.net.http.ContentType
import groovyx.net.http.Method

/**
 * Created by peto.robert on 2015.12.13..
 */
class UploadContractsJob {

    def live = false

    void upload() {
        def wsDownloader = new WsDownloader()
        def connector = new RestConnector("http://192.168.1.13:8080/TriDoc/login")
        connector.httpBuilder.request(Method.POST, ContentType.XML) {
            body = wsDownloader.authenticate()
        }

        connector.httpBuilder.request(Method.POST, ContentType.TEXT) {
            uri.path = "http://192.168.1.13:8080/TriDoc/report/tridoc/docList"
            body = DOWNLOAD_CONTRACTS
            response.success = { resp, xml ->
                TriDokContractParser parser = TriDokContractParser.buildDefaultParser()
                List<Contract> tridokContracts = parser.parseContractChunks(xml)
                List<Contract> pdeContracts = new DownloadContractScript().downloadContractsAfter(null)
                MergeContractList merge = new MergeContractList()

                merge.merge(pdeContracts, tridokContracts)

                merge.pde.each { Contract contract ->
                    println "Sending contract to tridok: $contract"


                    if (contract.mayImportable()) {
                        parser.partners = allTriDok
                        String contractXml = parser.generate(contract)
                        println contractXml
                        if (live) {
//                    connector.httpBuilder.request(Method.POST, ContentType.TEXT) {
//                        uri.path = "http://192.168.1.13:8080/TriDoc/report/tridoc/doc"
//                       body = conrtactXml
//                    }
                        }
                    }
                }
            }
            headers = ["Accept": "application/text", "Content-Type": "application/text; charset=utf-8", "Cookie": connector.cookies.join(';')]
        }
    }


    static final String DOWNLOAD_CONTRACTS = """
<request>
  <action>show</action>
  <entity>
        <tridoc:docListParam xmlns:tridoc="http://www.trilobita.hu/schema/tridoc/model-1_0_0">
            <tridoc:docType><tridoc:id>193</tridoc:id></tridoc:docType>
            <tridoc:docExpired>false</tridoc:docExpired>
            <tridoc:showCustomFields>true</tridoc:showCustomFields>
        </tridoc:docListParam>
  </entity>
</request>
"""

    ///////////////////////////////////////////////////////////////////////////////////////

    static String letters = "'\"„ASDFGHJKLQWERTYUIOPZXCVBNM"

    List<Partner> allTriDok = new ArrayList<>()
    def wsDownloader = new WsDownloader()
    def connector = new RestConnector("http://192.168.1.13:8080/TriDoc/login")

    void sync() {
        connector.httpBuilder.request(Method.POST, ContentType.XML) {
            body = wsDownloader.authenticate()
        }

        sync(0)
    }

    void syncDone() {
        println "ALL: ${allTriDok.size()}"
        upload();
    }

    void sync(int i) {
        //  println "Sync ${letters[i]}"

        connector.httpBuilder.request(Method.POST, ContentType.TEXT) {
            uri.path = "http://192.168.1.13:8080/TriDoc/report/tripartner/firmList"
            body = """
<request>
  <action>show</action>
  <entity>
    <tridoc:firmListParam xmlns:tridoc="http://www.trilobita.hu/schema/tripartner/model-1_0_0">
                    <tridoc:name><![CDATA[${letters[i]}%]]></tridoc:name>
                    <tridoc:showCustomFields>true</tridoc:showCustomFields>
    </tridoc:firmListParam>
  </entity>
</request>
"""
            response.success = { resp, xml ->
                TriDokPartnerParser parser = TriDokPartnerParser.buildDefaultParser()
                List<Partner> tridokPartners = parser.parsePartners(xml)
                allTriDok.addAll(tridokPartners)
                if (i < letters.length() - 1) {
                    sync(i + 1)
                } else {
                    syncDone()

                }
                headers = ["Accept": "application/text", "Content-Type": "application/text; charset=utf-8", "Cookie": connector.cookies.join(';')]
            }
        }
    }


}
