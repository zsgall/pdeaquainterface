package com.proconx.pdent.tridocmodule.job.pde

import com.proconx.pdent.tridocmodule.Contract
import com.proconx.pdent.tridocmodule.db.scripts.DownloadContractScript

/**
 * Created by peto.robert on 2016.01.22..
 */
class SelectContracts {

    List<Contract> pdeContracts = new ArrayList<>()

    void selectContracts() {
        pdeContracts = new DownloadContractScript().downloadContractsAfter(null)
    }
}
