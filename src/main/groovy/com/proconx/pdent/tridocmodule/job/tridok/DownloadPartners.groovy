package com.proconx.pdent.tridocmodule.job.tridok

import com.proconx.pdent.tridocmodule.Partner
import com.proconx.pdent.tridocmodule.wsclient.TriDokPartnerParser
import com.proconx.pdent.tridocmodule.wsclient.WsDownloader
import com.proconx.pdent.tridocmodule.wsclient.connector.RestConnector
import groovyx.net.http.ContentType
import groovyx.net.http.Method

/**
 * Created by peto.robert on 2016.01.21..
 */
class DownloadPartners {

    static String letters = "'\"„ASDFGHJKLQWERTYUIOPZXCVBNM0123456789"

    List<Partner> tridokPartners = new ArrayList<>()
    def wsDownloader = new WsDownloader()
    def connector = new RestConnector("http://192.168.1.13:8080/TriDoc/login")

    void auth() {
        connector.httpBuilder.request(Method.POST, ContentType.XML) {
            body = wsDownloader.authenticate()
        }
    }

    void sync(startsWith) {


        connector.httpBuilder.request(Method.POST, ContentType.TEXT) {
            uri.path = "http://192.168.1.13:8080/TriDoc/report/tripartner/firmList"
            body = """
                    <request>
                      <action>show</action>
                      <entity>
                        <tridoc:firmListParam xmlns:tridoc="http://www.trilobita.hu/schema/tripartner/model-1_0_0">
                                        <tridoc:name><![CDATA[${startsWith}%]]></tridoc:name>
                                        <tridoc:showCustomFields>true</tridoc:showCustomFields>
                        </tridoc:firmListParam>
                      </entity>
                    </request>
                    """
            response.success = { resp, xml ->
                TriDokPartnerParser parser = TriDokPartnerParser.buildDefaultParser()
                List<Partner> partners = parser.parsePartners(xml)
                println "DONE: ${startsWith} (${partners.size()})"

                if (partners.size() == 100) {
                    throw new Exception("Tridok returned 100 partner with letter '${startsWith}'. Possible data loss");
                }
                tridokPartners.addAll(partners)
                headers = ["Accept": "application/text", "Content-Type": "application/text; charset=utf-8", "Cookie": connector.cookies.join(';')]
            }
        }
    }

    public void downloadPartners() {
        auth()
        for (int i = 1; i < letters.size(); i++) {
            for (int j = 1; j < letters.size(); j++) {
                sync("${letters[i]}${letters[j]}")
            }
        }

//        (1..letters.size() - 1).forEach { Integer i ->
//            (1..letters.size() - 1).forEach { Integer j ->
//                sync("${letters[i]}${letters[j]}")
//            }
//        }
        println "FULL: (${tridokPartners.size()})"
        //  tridokPartners.each { TriDokPartner p -> println "PPP: ${p.name}"}
    }

    public static void main(String[] args) {
        DownloadPartners dtp = new DownloadPartners()
        dtp.downloadPartners()
    }
}
