package com.proconx.pdent.tridocmodule.job.pde

import com.proconx.pdent.tridocmodule.Invoice
import com.proconx.pdent.tridocmodule.db.scripts.DownloadInvoiceScript

/**
 * Created by peto.robert on 2016.01.22..
 */
class SelectInvoices {

    List<Invoice> pdeInvoices

    void selectInvoiceChunks() {
        pdeInvoices = new DownloadInvoiceScript().downloadInvoiceChunks()
        println "PDE Invoice ${pdeInvoices.size()}"

    }
}
