package com.proconx.pdent.tridocmodule.job;

/**
 * Created by peto.robert on 2018.07.05..
 */
public enum InvoiceType {
    AG(75, "AG"), ME(76, "ME"), AE(77, "AE"), EG_KP(190, "EG_KP"), ME_KP(191, "ME_KP"), AE_KP(192, "AE_KP");

    final String name;
    final int code;

    InvoiceType(int code, String name) {
        this.code = code;
        this.name = name;
    }
}
