package com.proconx.pdent.tridocmodule.job.tridok

import com.proconx.pdent.tridocmodule.Partner
import com.proconx.pdent.tridocmodule.wsclient.TriDokPartnerParser
import com.proconx.pdent.tridocmodule.wsclient.WsDownloader
import com.proconx.pdent.tridocmodule.wsclient.connector.RestConnector
import groovyx.net.http.ContentType
import groovyx.net.http.Method

/**
 * Created by peto.robert on 2016.01.29..
 */
class UploadPartner {

    def wsDownloader = new WsDownloader()
    def connector = new RestConnector("http://192.168.1.13:8080/TriDoc/login")

    void auth() {
        connector.httpBuilder.request(Method.POST, ContentType.XML) {
            body = wsDownloader.authenticate()
        }
    }


    void uploadPartner(Partner partner) {
        TriDokPartnerParser parser = TriDokPartnerParser.buildDefaultParser()
        String conrtactXml = parser.generate(partner)
        println "$conrtactXml"
        connector.httpBuilder.request(Method.POST, ContentType.TEXT) {
            uri.path = "http://192.168.1.13:8080/TriDoc/entity/tripartner/firm"
            body = conrtactXml
        }
    }
}
