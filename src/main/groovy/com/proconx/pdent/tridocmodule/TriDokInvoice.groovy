package com.proconx.pdent.tridocmodule

import com.proconx.pdent.tridocmodule.db.scripts.WorknumScript
import com.proconx.pdent.tridocmodule.job.full.FullData

/**
 * Created by szpetip on 27/11/15.
 */
class TriDokInvoice extends Invoice {

    boolean partnerMissing

    boolean ownCompanyMissing

    boolean partnerFromDB

    String partner

    String ownCompany

    Double price;

    Long triDokDocId

    TriDokInvoice() {
        currency = "HUF"
        rate = 1
    }

    void updateInvoice() {

        if (ownCompany == null) ownCompanyMissing = true
        if (partner == null) partnerMissing = true
        if (worknum == null) worknumMissing = true


        if (rate == 0.0D || rate == 1.0D) {
            priceInHuf = price
            priceInDev = price
            currency = "HUF"
        } else {
            priceInHuf = priceInDev / rate
            priceInDev = price
        }

        if (type == SUPPLIER) {
            supplier = ownCompany // + ( ownCompanyMissing ? " ${Invoice.MISSING}" : "" )
            customer = partner // + ( partnerMissing ? " ${Invoice.MISSING}" : "" )
        } else {
            supplier = partner // + ( partnerMissing ? " ${Invoice.MISSING}" : "" )
            customer = ownCompany // + ( ownCompanyMissing ?" ${Invoice.MISSING}" : "" )
        }

        if (worknum != null) {
            worknum = worknum //+ ( worknumMissing ? " ${Invoice.MISSING}" : "" )
        }

        //    supplier = "Aqua-General Kft."
        //    customer = "Magyar Posta"
    }

    public boolean mayImportable() {

        Long pdeProjectId = new WorknumScript().getProjectIdByCustId(this.worknum)

        // Worknum wn = FullData.db().downloadWorknums.tridokWorknums.find { Worknum wn -> wn.custid == this.worknum}
        Partner supp = FullData.db().downloadPartners.tridokPartners.find { Partner partner -> partner.name == this.supplier }
        Partner cust = FullData.db().downloadPartners.tridokPartners.find { Partner partner -> partner.name == this.customer }

        if (pdeProjectId == null) {
            worknum = "${worknum} ${Invoice.MISSING}"
        }

        if (supp == null) {
            this.supplier = "${this.supplier} ${Invoice.MISSING}"
        }

        if (cust == null) {
            this.customer = "${this.customer} ${Invoice.MISSING}"
        }


        return pdeProjectId != null && supp != null && cust != null
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "importable='" + mayImportable() + "'" +
                ", custid='" + custid + '\'' +
                ", type='" + type + '\'' +
                ", +ownCompany='" + ownCompany + '\'' +
                ", +partner='" + partner + '\'' +
                ", supplier='" + supplier + '\'' +
                ", customer='" + customer + '\'' +
                ", worknum='" + worknum + '\'' +
                ", contract='" + contract + '\'' +
                ", dateOfCompletion=" + dateOfCompletion +
                ", dateOfCreation=" + dateOfCreation +
                ", dateOfPaymentDeadline=" + dateOfPaymentDeadline +
                ", dateOfPayment=" + dateOfPayment +
                ", currency='" + currency + '\'' +
                ", rate=" + rate +
                ", +price=" + price +
                ", priceInDev=" + priceInDev +
                ", priceInHuf=" + priceInHuf +
                ", vat=" + vat +
                ", vatType=" + vatType +
                '}';
    }
}
