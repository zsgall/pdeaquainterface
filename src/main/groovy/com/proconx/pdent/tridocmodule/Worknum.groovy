package com.proconx.pdent.tridocmodule

/**
 * Created by peto.robert on 2016.01.22..
 */
class Worknum {
    Long id
    String custid

    String normalizedCustid() {
        return normalizeCustId(custid)
    }

    static String normalizeCustId(String custid) {
        int lastSpace = custid.indexOf(" ")
        return lastSpace > 1 ? custid.substring(lastSpace - 1) : custid
    }
}
