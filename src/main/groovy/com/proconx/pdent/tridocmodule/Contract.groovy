package com.proconx.pdent.tridocmodule

import com.proconx.pdent.tridocmodule.job.full.FullData
import com.proconx.pdent.tridocmodule.job.tridok.ContractType

/**
 * Created by szpetip on 10/11/15.
 */
class Contract {

    static String TYPE_ALVALLALKOZOI = "Alvállalkozói"
    static String TYPE_MEGRENDELOI = "Megrendelői"

    static String DATE_OF_WAR_DAY = "nap"
    static String DATE_OF_WAR_MONTH = "hónap"
    static String DATE_OF_WAR_YEAR = "év"

    ContractType contractType

    /* SZERZODES SZAM(K): szerződés azonosítója, EGYEDI */
    String custid

    /* TIPUS(K): az alábbi értékeket veheti fel: (Alvállalkozói szerződés, Megrendelői szerződés) */
    String type

    /* TÁRGY(K): a szerződés tárgya */
    String name

    /* MUNKASZAM(K): a projekt munkaszáma */
    String worknum

    /* MEGBIZO(K): a megbízó partnerkódja */
    String partner

    /* MEGBIZOTT(K): a megbízott partnerkódja */
    String contractor

    /* MEGKOTES(K): dátum, óra, perc nélkül (a szerződés kelte) */
    Date dateOfContract

    /* KEZDES: dátum, óra, perc nélkül */
    Date dateOfStart

    /* BEFEJEZES: dátum, óra, perc nélkül */
    Date dateOfFinish

    /* ERTEK(K): Ha nincs érték, akkor legyen 0 */
    Double price

    String currency = "HUF"

    /* ADO MERTEK(K): áfalulcs százalék */
    Double vat

    /* ADO TIPUS(K): 1 áfás, 0 fordított áfás */
    String vatType

    /* FIZETÉSI HATÁRIDŐ: naptári vagy munkanapban megadva */
    Date dateOfPaymentDeadline

    /* FIZETÉSI HATÁRIDŐ: naptári vagy munkanapban megadva */
    Integer dayOfPaymentDeadline

    /* ELŐLEG ÖSSZEGE: az előleg értéke */
    Double advancedValue

    /* ELŐLEG KIFIZETÉSÉNEK DÁTUMA: dátum, óra, perc nélkül */
    Date dateOfAdvancedValuePayment

    /* TELJESITESI VISSZATARTAS ARÁNY: százalékban vagy forintban */
    Double retPerc

    /* TELJESITESI VISSZATARTAS DATUM: dátum, óra, perc nélkül */
    Date dateOfRetPerc

    /* GARANCIALIS VISSZATARTAS ARANY: százalékban vagy forintban */
    Double warPerc

    /* GARANCIALIS VISSZATARTAS LEJARATA: nap, hónap vagy év megadása */
    Integer deadlineOfWar
    String deadlineOfWarUnit

    /* MEGRENDELŐI SZOLGÁLTATÁS: százalék vagy forint */
    Double serviceValue

    String tridokName() {
        return toTridokName(name)
    }

    static String toTridokName(String name) {
        return "PDE szerződés $name"
    }

    void copyTo(Contract contract) {
        contract.custid = custid
        contract.type = type
        contract.worknum = worknum
        contract.partner = partner
        contract.contractor = contractor
        contract.dateOfContract = dateOfContract
        contract.dateOfStart = dateOfStart
        contract.dateOfFinish = dateOfFinish
        contract.price = price
        contract.vat = vat
        contract.vatType = vatType
        contract.dayOfPaymentDeadline = dayOfPaymentDeadline
        contract.advancedValue = advancedValue
        contract.dateOfAdvancedValuePayment = dateOfAdvancedValuePayment
        contract.retPerc = retPerc
        contract.dateOfRetPerc
        contract.deadlineOfWar = deadlineOfWar
        contract.deadlineOfWarUnit = deadlineOfWarUnit
        contract.serviceValue = serviceValue
        if (contractType != null) contract.contractType = contractType
    }

    @Override
    public String toString() {
        return "Contract{" +
                "importable='" + mayImportable() + "'" +
                ", custid='" + custid + '\'' +
                ", type='" + type + '\'' +
                ", contractType='" + contractType + '\'' +
                ", name='" + name + '\'' +
                ", worknum='" + worknum + '\'' +
                ", partner='" + partner + '\'' +
                ", contractor='" + contractor + '\'' +
                ", dateOfContract=" + dateOfContract +
                ", dateOfStart=" + dateOfStart +
                ", dateOfFinish=" + dateOfFinish +
                ", price=" + price +
                ", vat=" + vat +
                ", vatType='" + vatType + '\'' +
                ", dateOfPaymentDeadline=" + dateOfPaymentDeadline +
                ", dayOfPaymentDeadline=" + dayOfPaymentDeadline +
                ", advancedValue=" + advancedValue +
                ", dateOfAdvancedValuePayment=" + dateOfAdvancedValuePayment +
                ", retPerc=" + retPerc +
                ", dateOfRetPerc=" + dateOfRetPerc +
                ", warPerc=" + warPerc +
                ", deadlineOfWar=" + deadlineOfWar +
                ", deadlineOfWarUnit='" + deadlineOfWarUnit + '\'' +
                ", serviceValue=" + serviceValue +
                '}';
    }

    public boolean mayImportable() {
        Worknum wn = FullData.db().downloadWorknums.tridokWorknums.find { Worknum wn -> wn.custid == this.worknum }
        Partner partner = FullData.db().downloadPartners.tridokPartners.find { Partner partner -> partner.name == this.partner }

        if (wn == null) {
            worknum = "${worknum} ${Invoice.MISSING}"
        }

        if (partner == null) {
            this.partner = "${this.partner} ${Invoice.MISSING}"
        }

        return wn != null && partner != null && contractType != null
    }
}
