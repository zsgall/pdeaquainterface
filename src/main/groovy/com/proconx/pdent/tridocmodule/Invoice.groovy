package com.proconx.pdent.tridocmodule
/**
 * Created by szpetip on 10/11/15.
 */
class Invoice {

    boolean worknumMissing

    static final String CUSTOMER = "CUSTOMER"

    static final String SUPPLIER = "SUPPLIER"

    /* SZÁMLA AZONOSÍTÓ (K): számla azonosítója, EGYEDI */
    String custid;

    String name;

    /* SZÁMLA TIPUS (K): A következő értékeket veheti fel: CUSTOMER: Kimenő számla, SUPPLIER: Bejövő számla */
    String type;

    /* KIBOCSÁTÓ (K) */
    String supplier;

    /* BEFOGADÓ (H): Nem kötelező, ahol nincs megadva, azok a „KEZI” partnerkóddal töltődnek be */
    String customer;

    /* MUNKASZÁM(K): a projekten lévő munkaszám */
    String worknum;

    /* SZERZODES: a szerződés kódja - hozzácsatolható */
    String contract;

    /* TELJ DATUM(K): teljesítés dátum */
    Date dateOfCompletion;

    /* SZLA KELTE(K): kiállítás dátum */
    Date dateOfCreation;

    /* FIZETÉSI HATÁRIDŐ(K): fizetési határidő */
    Date dateOfPaymentDeadline;

    /* KIEGYENLÍTÉS DÁTUMA: kifizetés ideje */
    Date dateOfPayment;

    /* DEVIZA: devizakód */
    String currency;

    /* ÁRFOLYAM: árfolyam */
    Double rate;

    /* NETTO_DEVIZA: nettó érték devizában */
    Double priceInDev;

    /* NETTO_HUF(K): nettó érték forintban */
    Double priceInHuf = 0.0

    /* ADÓ MÉRTÉKE */
    Double vat = 0.0

    /* ADÓ TÍPUS */
    int vatType;


    public boolean mayImportable() {
        return !worknumMissing
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "importable='" + mayImportable() + "'" +
                ", custid='" + custid + '\'' +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", supplier='" + supplier + '\'' +
                ", customer='" + customer + '\'' +
                ", worknum='" + worknum + '\'' +
                ", contract='" + contract + '\'' +
                ", dateOfCompletion=" + dateOfCompletion +
                ", dateOfCreation=" + dateOfCreation +
                ", dateOfPaymentDeadline=" + dateOfPaymentDeadline +
                ", dateOfPayment=" + dateOfPayment +
                ", currency='" + currency + '\'' +
                ", rate=" + rate +
                ", priceInDev=" + priceInDev +
                ", priceInHuf=" + priceInHuf +
                ", vat=" + vat +
                ", vatType=" + vatType +
                '}';
    }

    static final String MISSING = "(HIÁNYZIK)"
    static final String NOK = "(NO)"
    static final String OK = "OK"

}
