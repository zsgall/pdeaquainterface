package com.proconx.pdent.tridocmodule.html

import com.proconx.pdent.tridocmodule.Invoice

import java.text.SimpleDateFormat

/**
 * Created by peto.robert on 2015.12.17..
 */
class HtmlTableView {

    List<Object> data


    String rowsHtml() {
        StringBuilder sbr = new StringBuilder()

        rows().each { List r ->
            sbr.append("<tr>")
            int i = 0;
            r.each {
                boolean important = importantColumns().contains(i)
                sbr.append("<td class='${it.toString() == 'null' || it.toString()?.indexOf(Invoice.MISSING) >= 0 || it.toString()?.indexOf(Invoice.NOK) >= 0 ? (!important ? 'classNull' : 'classNullImportant') : ''}' >${it?.toString()}</td>")
                i++
            }
            sbr.append("</tr>")


        }
        sbr.toString()
    }

    String title() {
        return "HTML Report"
    }

    String html() {

        StringBuilder sbh = new StringBuilder()
        sbh.append("<tr>")

        headers().each { String h ->
            sbh.append("<td>$h</td>")
        }
        sbh.append("</tr>")

        String rowsString = rowsHtml()





        """
<html>
<head>
<meta charset="UTF-8">
<title>${title()}</title>
</head>
<body>
<style>
.classNull {
background: #cecece;
}
.classNullImportant {
background: #ff0000;
}
</style>
<h1>${title()}</h1>
<table border=1>
$sbh
$rowsString
</table>
</body>
</html>
"""

    }

    String[] headers() {
        return []
    }

    List rows() {
        return []
    }

    Integer[] importantColumns() {
        return []
    }

    String dateStr(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd")
        return date != null ? sdf.format(date) : "null"
    }
}
