package com.proconx.pdent.tridocmodule.html

import com.proconx.pdent.tridocmodule.Invoice
import com.proconx.pdent.tridocmodule.TriDokInvoice

/**
 * Created by peto.robert on 2016.01.04..
 */
class InvoiceHtmlTableView extends HtmlTableView {

    List<Invoice> invoices


    @Override
    String[] headers() {
        return ["SZÁMLA AZONOSÍTÓ", "SZÁMLA TIPUS", "KIBOCSÁTÓ", "BEFOGADÓ", "MUNKASZÁM", "TELJ DATUM", "SZLA KELTE", "FIZETÉSI HATÁRIDŐ",
                "KIEGYENLÍTÉS DÁTUMA", "DEVIZA", "ÁRFOLYAM", "NETTO_DEVIZA", "NETTO_HUF", "ADÓ MÉRTÉKE", "ADÓ TÍPUS", "IMPORTÁLHATÓ"]
    }

    @Override
    List rows() {


        invoices.collect { TriDokInvoice inv ->
            [inv.custid, inv.type, inv.supplier + (!inv.partnerFromDB ? " (KÉZI)" : ""), inv.customer, inv.worknum, dateStr(inv.dateOfCompletion), dateStr(inv.dateOfCreation), dateStr(inv.dateOfPaymentDeadline), dateStr(inv.dateOfPayment),
             inv.currency, inv.rate, inv.priceInDev, inv.priceInHuf, inv.vat, inv.vatType == 0 ? "NORM" : "FORD", inv.mayImportable() ? "OK" : Invoice.NOK]
        }
    }

    @Override
    String title() {
        "Számlák TRIDOK -> PDE ${dateStr(new Date())}"
    }

    @Override
    Integer[] importantColumns() {
        return [0, 1, 2, 3, 4, 15]
    }
}
