package com.proconx.pdent.tridocmodule.html

import com.proconx.pdent.tridocmodule.Contract
import com.proconx.pdent.tridocmodule.Invoice

/**
 * Created by peto.robert on 2016.01.04..
 */
class ContractHtmlTableView extends HtmlTableView {

    List<Contract> contracts


    @Override
    String[] headers() {
        return ["MUNKASZÁM", "TÍPUS", "IKTATAS", "SZERZ. AZ.", "CÍM", "SAJ. CÉG", "PARTNER", "SZERZ. DÁTUMA", "KEZDET", "VÉG", "ÉRTÉK", "ÁFA", "IMPORTÁLHATÓ"]
    }

    @Override
    List rows() {
        contracts.collect { Contract con ->
            con.mayImportable()
            [con.worknum, con.type, con.contractType, con.custid, con.name, con.contractor, con.partner, dateStr(con.dateOfContract), dateStr(con.dateOfStart), dateStr(con.dateOfFinish), con.price, con.vat, con.mayImportable() ? "OK" : Invoice.NOK]
        }
    }

    @Override
    String title() {
        "Szerződések PDE -> TRIDOK ${dateStr(new Date())}"
    }

    @Override
    Integer[] importantColumns() {
        return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    }
}
