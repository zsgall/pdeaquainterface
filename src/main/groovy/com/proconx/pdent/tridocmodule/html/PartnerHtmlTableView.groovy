package com.proconx.pdent.tridocmodule.html

import com.proconx.pdent.tridocmodule.MergedPartner
import com.proconx.pdent.tridocmodule.Partner


/**
 * Created by peto.robert on 2016.01.15..
 */
class PartnerHtmlTableView extends HtmlTableView {

    List<Partner> partners

    @Override
    String[] headers() {
        return ["TRIDOK AZ", "PDE AZ", "PDE KOD", "NÉV", "CEGJ", "ADÓSZÁM", "SZLA SZÁM1", "SZLA SZÁM2", "SZLA SZÁM3", "MKIK",
                "MKKIR", "VÁROS", "CÍM1", "CÍM2", "IR.SZ", "ORSZÁG", "KÉPVISELŐ", "Email", "Telefon", "Fax", "Mobil", "WEB"]
    }

    String triplet(def m, def p, def c) {
        return "${m}<br>${p}<br>${c}"
    }

    @Override
    List rows() {


        partners.collect { Partner p ->
            String tridokid = "-"
            if (p instanceof Partner) {
                tridokid = p.tridokId?.toString()
            }
            if (p instanceof MergedPartner) {
                MergedPartner m = p
                [tridokid, p.pdeId, p.custid, p.name, m.triplet("compid"), m.triplet("vatNumber"), m.triplet("accountNumber1"), m.triplet("accountNumber2"), m.triplet("accountNumber3"),
                 m.triplet("mkik"), m.triplet("mkkir"), m.triplet("city"), m.triplet("address1"), m.triplet("address2"), m.triplet("postalNumber"), m.triplet("country"), m.triplet("contractPerson"), m.triplet("email"), m.triplet("phone"), m.triplet("fax"), m.triplet("mobile"), m.triplet("web")]

            } else {
                [tridokid, p.pdeId, p.custid, p.name, p.compid, p.vatNumber, p.accountNumber1, p.accountNumber2, p.accountNumber3,
                 p.mkik, p.mkkir, p.city, p.address1, p.address2, p.postalNumber, p.country, p.contractPerson, p.email, p.phone, p.fax, p.mobile, p.web]
            }
        }
    }


    @Override
    String title() {
        "Partnerek PDE <-> TRIDOK ${dateStr(new Date())}"
    }

    @Override
    Integer[] importantColumns() {
        return [0, 1, 2, 3, 4]
    }
}
