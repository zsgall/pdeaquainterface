package com.proconx.pdent.tridocmodule

/**
 * Created by szpetip on 26/11/15.
 */
class CustomField {

    String key
    String value

    String toString() {
        return "{$key: $value}"
    }
}
