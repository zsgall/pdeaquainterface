package com.proconx.pdent.tridocmodule.db.connection

import groovy.sql.Sql

import java.sql.DriverManager

/**
 * Created by szpetip on 10/11/15.
 */
class DBConnection {
    def url = "jdbc:oracle:thin:@192.168.1.12:1521:orcl"
    def user = "pdeaqua"
    def password = "pdeaqua246"
    def driver = "oracle.jdbc.driver.OracleDriver"

    private Sql sql;

    private static connection;

    Sql getSql2() {
        Class.forName(driver)
        Class.forName("oracle.xdb.XMLType")
        def conn = DriverManager.getConnection(url, user, password)
        conn.setNetworkTimeout(null, 10000)
        def sql = new Sql(conn)
        return sql
    }

    Sql getSql() {
//        return getSql2()
//        if (sql != null) {
//            sql.close()
//        }
        def sql = Sql.newInstance(url, user, password, driver)
        sql.withStatement {
            stmt -> stmt.queryTimeout = 100
        }
        return sql
    }

    static DBConnection getConnection() {
        if (connection == null) {
            connection = new DBConnection()
        }
        return connection
    }
}
