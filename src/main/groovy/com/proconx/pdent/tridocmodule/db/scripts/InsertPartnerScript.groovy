package com.proconx.pdent.tridocmodule.db.scripts

import com.proconx.pdent.tridocmodule.Partner
import com.proconx.pdent.tridocmodule.db.connection.DBConnection

/**
 * Created by szpetip on 12/11/15.
 */
class InsertPartnerScript extends BaseDBScript {

    void insertPartner(Partner partner) {

        Boolean needsParent = partner.pdeId != null && partner.pdeId != 0L

        String parentQuery = needsParent ? "(select pdo_id from organizations where custid='${Partner.CUSTID_TRIDOCIMPORT}')" : "null"


        Integer pdoId = getPdoId()
        String script = """
            insert into organizations (
                pdo_id,
                custid,
                extId,
                name,
                compid,
                dutynum,
                accountnum,
                accountnum2,
                accountnum3,
                regnum,
                regnum2,
                city,
                addr1,
                country,
                zipcode,
                email,
                phone,
                phone2,
                org_pdo_id,
                cit_id)
            select
                PDE_OBJECT_SEQ.nextval,
                ${str partner.custid},
                ${str partner.extId},
                ${str partner.name},
                ${str partner.compid},
                ${str partner.vatNumber},
                ${str partner.accountNumber1},
                ${str partner.accountNumber2},
                ${str partner.accountNumber3},
                ${str partner.mkik},
                ${str partner.mkkir},
                ${str partner.city},
                ${str partner.address1},
                ${str partner.country},
                ${str partner.postalNumber},
                ${str partner.email},
                ${str partner.phone},
                ${str partner.mobile},
                (select pdo_id from organizations where custid='${Partner.CUSTID_TRIDOCIMPORT}'),
                (select min(id) from cities c where c.postalcode=${str partner.postalNumber} and c.name=${str partner.city} )
             from dual where not exists (select * from organizations o where o.custid=${
            str partner.custid
        } and o.name = ${str partner.name})"""
        DBConnection connection = DBConnection.getConnection()
        def sql = connection.getSql()

        try {
            sql.execute(script)
        } catch (Exception ex) {
            sql.rollback();
        } finally {
            if (sql != null) {
                sql.close();
            }
        }

    }
}
