package com.proconx.pdent.tridocmodule.db.scripts

/**
 * Created by szpetip on 10/11/15.
 */
class WorknumScript extends BaseDBScript {

    Integer getWorknumIdByCustId(String custid) {
        return integerResult("select id as result from worknums where custid='$custid'")
    }

    Integer getProjectIdByCustId(String custid) {
        return integerResult("select pdo_id as result from projects where custid='$custid'")
    }

    Integer getWorknumIdByPdoId(String custid) {
        return integerResult("select max(id) as result from worknums where pdo_id=(select pdo_id from projects where custid='$custid' )")
    }


}
