package com.proconx.pdent.tridocmodule.db.scripts

import com.proconx.pdent.tridocmodule.Invoice
import com.proconx.pdent.tridocmodule.db.connection.DBConnection

/**
 * Created by peto.robert on 2015.12.15..
 */
class DownloadInvoiceScript extends BaseDBScript {


    List<Invoice> downloadInvoiceChunks() {
        List<Invoice> invoices = new ArrayList<>()

        def script =
                """
select i.custid as "custid",
i.name as "name",
(select p.custid from projects p where p.pdo_id=i.pdo_id_belongs_to ) as "worknum"
from invoices i
"""

//                """
//select i.custid as "custid",
//i.name as "name",
//(select p.custid from projects p where p.id=i.pro_pdo_id ) as "worknum"
//from invoices i
//"""
        connection = DBConnection.getConnection()
        def sql = connection.getSql()
        try {
            sql.eachRow(script) { row ->
                // println("CLASS: ${row}")

                Invoice invoice = new Invoice(row.toRowResult() as Map)
                invoices.add(invoice)
            }
        } catch (Exception ex) {
            System.err.println("EX" + ex)
        } finally {
            sql.close()
        }
        return invoices
    }
}
