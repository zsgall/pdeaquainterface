package com.proconx.pdent.tridocmodule.db.scripts

/**
 * Created by szpetip on 10/11/15.
 */
class OrganizationScript extends BaseDBScript {


    public Integer getOrganizationIdByName(String organizationName) {
        return integerResult("select pdo_id as result from organizations where name='$organizationName'")
    }

    public Integer getOrganizationOrDefaultIdByName(String organizationName) {
        return integerResult("select o.pdo_id as result from organizations o where o.name='$organizationName' union select o2.pdo_id from organizations o2 where o2.custid='KEZI' and NOT EXISTS (select o.pdo_id as result from organizations o where o.name='$organizationName')")
    }

}
