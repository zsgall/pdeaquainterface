package com.proconx.pdent.tridocmodule.db.scripts

import com.proconx.pdent.tridocmodule.Partner
import com.proconx.pdent.tridocmodule.db.connection.DBConnection

/**
 * Created by szpetip on 12/11/15.
 */
class UpdatePartnerScript extends BaseDBScript {

    void updatePartner(Partner partner) {

        String script = """
            update organizations set
                name = ${str partner.name},
                compid = ${str partner.compid},
                dutynum = ${str partner.vatNumber},
                accountnum = ${str partner.accountNumber1},
                accountnum2 = ${str partner.accountNumber2},
                accountnum3 = ${str partner.accountNumber3},
                regnum = ${str partner.mkik},
                regnum2 = ${str partner.mkkir},
                city = ${str partner.city},
                addr1 = ${str partner.address1},
                country = ${str partner.country},
                zipcode = ${str partner.postalNumber},
                email = ${str partner.email},
                phone = ${str partner.phone},
                phone2 = ${str partner.mobile}
            WHERE custid=${str partner.custid}
"""
        DBConnection connection = DBConnection.getConnection()
        def sql = connection.getSql()

        try {
            println("EXECUTING:\r\n$script")
            sql.execute(script)
        } finally {
            if (sql != null) {
                sql.close();
            }
        }
    }
}
