package com.proconx.pdent.tridocmodule.db.scripts

import com.proconx.pdent.tridocmodule.Contract
import com.proconx.pdent.tridocmodule.db.connection.DBConnection
import com.proconx.pdent.tridocmodule.job.tridok.ContractType

/**
 * Created by szpetip on 11/11/15.
 */
class DownloadContractScript extends BaseDBScript {

    List<Contract> downloadContractsAfter(Date date) {
        List<Contract> contracts = new ArrayList<>()

        def script =
                """
select c.custid as \"custid\",
  (select di.name from dictionary_items di where di.dic_pdo_id = c.dit_dic_pdo_id and di.id = c.dit_id ) as \"type\",
  c.name as \"name\",
  (select p.custid from projects p where p.pdo_id=c.pro_pdo_id ) as \"worknum\",
  (select name from organizations o where o.pdo_id=c.org_pdo_id) as \"contractor\",
  (select name from organizations o where o.pdo_id=c.org_pdo_id_agent) as \"partner\",
  c.contr_date as \"dateOfContract\",
  c.contr_start as \"dateOfStart\",
  c.contr_finish as \"dateOfFinish\",
  c.CONTR_PRICE as \"price\",
  (case c.reverse_vat when 1 then 0 else nvl(c.FAD_VAT,0) end) as "vat",
  c.DEADLINE_OF_PAYMENT as \"dayOfPaymentDeadline\",
  c.ADV_VALUE as \"advancedValue\",
  c.ADV_DATE as \"dateOfAdvancedValuePayment\",
  c.RET_PERC as \"retPerc\",
  c.RET_DATE as \"dateOfRetPerc\",
  c.WAR_PERC as \"warPerc\",
  c.DEADLINE_OF_WAR as \"deadlineOfWar\",
  c.UNIT_OF_DOW as \"deadlineOfWarUnit\"
from contracts c where c.pro_pdo_id not in (select g.pdo_id from group_items g where g.GRP_PDO_ID in (select pg.pdo_id from pdegroups pg where pg.custid='Teszt' or pg.custid='T1' or pg.custid= 'T2' or pg.custid= 'T3'))"""
        connection = DBConnection.getConnection()
        def sql = connection.getSql()
        try {
            sql.eachRow(script) { row ->
                // println("CLASS: ${row}")

                Contract contract = new Contract(row.toRowResult() as Map)

                String comp = ""

                if (contract.worknum?.startsWith("MM")) {
                    comp = "MM"

                } else if (contract.worknum?.startsWith("M")) {
                    comp = "AG"
                } else if (contract.worknum?.startsWith("AE")) {
                    comp = "AE"
                }

                if (contract.type != null) {
                    contract.contractType = ContractType.getBy(comp, contract.type)
                }

                if (contract.custid.startsWith("G-"))
                    contract.type = Contract.TYPE_ALVALLALKOZOI
                else
                    contract.type = Contract.TYPE_MEGRENDELOI

                //TODO: contract type!!!!

                contracts.add(contract)
            }
        } catch (Exception ex) {
            System.err.println("EX" + ex)
        } finally {
            sql.close()
        }
        return contracts
    }

}
