package com.proconx.pdent.tridocmodule.db.scripts

import com.proconx.pdent.tridocmodule.db.connection.DBConnection

import java.text.SimpleDateFormat

/**
 * Created by szpetip on 10/11/15.
 */

class BaseDBScript implements DBScript {

    DBConnection connection;


    Integer getPdoId() {
        return integerResult("select pde_object_seq.nextval as result from dual")
    }

    @Override
    Integer integerResult(String script) {
        connection = DBConnection.getConnection()
        def sql = connection.getSql()
        try {
            return sql.firstRow(script).get("RESULT") as Integer
        } catch (Exception ex) {
            //   System.err.println("EX: ${ex} SQL: ${script}")
            return null
        } finally {
            sql.close()
        }
    }

    String getDateAsSqlString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd")
        if (date != null) {
            "to_date('${sdf.format(date)}','yyyy-mm-dd')"
        } else {
            return "null"
        }
    }

    String str(String string) {
        if (string == null) {
            return "null"
        } else if (string == 'null') {
            return "null"
        } else if (string == '') {
            return "null"
        } else {
            return "'${string.replaceAll("\"", "\"\"").replaceAll("\'", "\'\'")}'"
        }
    }
}
