package com.proconx.pdent.tridocmodule.db.scripts

import com.proconx.pdent.tridocmodule.Invoice
import com.proconx.pdent.tridocmodule.TriDokInvoice
import com.proconx.pdent.tridocmodule.db.connection.DBConnection

import java.text.SimpleDateFormat

/**
 * Created by szpetip on 10/11/15.
 */
class InsertInvoiceScript extends BaseDBScript {

    WorknumScript worknumScript = new WorknumScript()
    OrganizationScript orgScript = new OrganizationScript()
    ContractScript contractScript = new ContractScript()

    void insertInvoice(TriDokInvoice invoice) {


        Integer belongsTo = worknumScript.getProjectIdByCustId(invoice.worknum)
        Integer orgPdoId = null;
        Integer orgPdoSenderId = null;
        String direction = "";
        if (invoice.type == Invoice.SUPPLIER) {
            orgPdoId = orgScript.getOrganizationIdByName(invoice.partner)
            orgPdoSenderId = orgScript.getOrganizationIdByName(invoice.ownCompany)
            direction = "D"

            if (orgPdoId == null || orgPdoId == 0) {
                println "Organization cannot be set: ${invoice.partner}"
            }

            if (orgPdoSenderId == null || orgPdoSenderId == 0) {
                println "Organization sender cannot be set: ${invoice.ownCompany}"
            }


        } else if (invoice.type == Invoice.CUSTOMER) {
            orgPdoId = orgScript.getOrganizationIdByName(invoice.ownCompany)
            orgPdoSenderId = orgScript.getOrganizationIdByName(invoice.partner)
            direction = "O"

            if (orgPdoId == null || orgPdoId == 0) {
                println "Organization cannot be set: ${invoice.ownCompany}"
            }

            if (orgPdoSenderId == null || orgPdoSenderId == 0) {
                println "Organization sender cannot be set: ${invoice.partner}"
            }


        }
        Integer conPdoId = contractScript.getContractIdByCode(invoice.contract)
        Integer wnId = worknumScript.getWorknumIdByPdoId(invoice.worknum)

        Integer pdoId = getPdoId()

        String dateOfCompStr = getDateAsSqlString(invoice.dateOfCompletion)
        String dateOfCreationStr = getDateAsSqlString(invoice.dateOfCreation)
        String dateOfPaymentDeadlineStr = getDateAsSqlString(invoice.dateOfPaymentDeadline)
        String dateOfPaymentStr = getDateAsSqlString(invoice.dateOfPayment)

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd")


        String innerId = "$direction+$belongsTo+$orgPdoId+$orgPdoSenderId"

        String scriptInvoice =
                """
        insert into invoices(pdo_id, pdo_id_belongs_to, org_pdo_id, org_pdo_id_sender, con_pdo_id, wn_id,
        date_of_send, date_of_invoice, deadline_of_pay, date_of_pay,
        name, custid,
        state, direction, currency, exchange_rate, extid, tri_doc_id)
        values($pdoId, $belongsTo, $orgPdoId, $orgPdoSenderId, $conPdoId, $wnId,
        $dateOfCompStr, $dateOfCreationStr, $dateOfPaymentDeadlineStr, $dateOfPaymentStr,
        '${invoice.custid}', '${invoice.custid}',
        1, '$direction', nvl('${invoice.currency}', 'HUF'),  ${invoice.rate}, '$innerId', ${invoice.triDokDocId})
        """

        if (invoice.vat == null) invoice.vat = 0.0
        if (invoice.priceInHuf == null) invoice.priceInHuf = 0.0

        //  System.err.println("XXX: ${invoice.vat} YYY: ${invoice.priceInHuf}")
        String scriptItems =
                """
        insert into invoice_items(inv_pdo_id, id, name, custid, amount, unit, price, vat, vatpercent, mod_date, is_percent, exchange_rate)
        values(
        $pdoId, (select nvl(max(id), 0)+1 from invoice_items where inv_pdo_id=$pdoId), 'TriDoc számlatétel', 'TRIDOC', 1, 'db',
                    ${invoice.priceInHuf / (1 + 0.01 * invoice.vat)}, ${
                    invoice.priceInHuf - (invoice.priceInHuf / (1 + 0.01 * invoice.vat))
                } , ${0.01 * invoice.vat}, sysdate, 0, ${invoice.rate}
        )
        """

        DBConnection connection = DBConnection.getConnection()
        def sql = connection.getSql()

        try {
            //   println("$scriptInvoice")
            sql.execute(scriptInvoice)
            //  println("SCRIPT: $scriptItems")
            sql.execute(scriptItems)
        } catch (Exception ex) {
            ex.printStackTrace()
        }
        finally {
            if (sql != null) {
                sql.close();
            }
        }

    }
}
