package com.proconx.pdent.tridocmodule.db.scripts

/**
 * Created by szpetip on 10/11/15.
 */
interface DBScript {

    public Integer integerResult(String script);

}
