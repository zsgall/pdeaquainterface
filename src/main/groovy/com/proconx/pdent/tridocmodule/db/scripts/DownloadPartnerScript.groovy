package com.proconx.pdent.tridocmodule.db.scripts

import com.proconx.pdent.tridocmodule.Partner
import com.proconx.pdent.tridocmodule.db.connection.DBConnection

/**
 * Created by szpetip on 12/11/15.
 */
class DownloadPartnerScript extends BaseDBScript {

    List<Partner> downloadPartners() {
        List<Partner> partners = new ArrayList<>()

        def script =
                """
select o.pdo_id as "pdeId",
o.custid as "custid",
o.name as "name",
o.compid as "compid",
dutynum as "vatNumber",
accountnum as "accountNumber1",
accountnum2 as "accountNumber2",
accountnum3 as "accountNumber3",
regnum as "mkik",
regnum2 as "mkkir",
c.name as "city",
addr1 as "address1",
addr2 as "address2",
country as "country",
c.postalcode as "postalNumber",
email as "email",
phone as "phone",
phone2 as "mobile",
extID as "extId"
from organizations o
left join cities c on o.cit_id = c.id
where o.custid != 'TRIDOCIMPORT' and o.name != 'PDE TESZT PARTNER' and o.name != 'PDE TESZT PARTNER 2'
"""
        connection = DBConnection.getConnection()
        def sql = connection.getSql()
        try {
            sql.eachRow(script) { row ->
                // println("CLASS: ${row}")

                Partner partner = new Partner(row.toRowResult() as Map)

                Partner chk = partners.find { it.custid == partner.custid }
                if (chk == null) {
                    partners.add(partner)
                }
            }
        } catch (Exception ex) {
            System.err.println("EX" + ex)
        } finally {
            sql.close()
        }
        return partners
    }

}
