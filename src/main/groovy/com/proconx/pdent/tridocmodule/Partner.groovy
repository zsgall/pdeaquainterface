package com.proconx.pdent.tridocmodule

/**
 * Created by szpetip on 12/11/15.
 */
class Partner {

    static final String CUSTID_TRIDOCIMPORT = "TRIDOCIMPORT"

    Long tridokId

    Long pdeId

    /* PARTNER (K): partner kód, EGYEDI, a partner azonosítója */
    String custid

    /* PARTNER NEVE (K): partner teljes neve */
    String name

    /* CEGJEGYZEKSZAM: cégjegyzékszám */
    String compid

    /* ADOSZAM: adószám */
    String vatNumber

    /* SZÁMLASZÁM: bankszámlaszám */
    String accountNumber1
    String accountNumber2
    String accountNumber3

    /* KAMARAI REG. SZÁM (MKIK): kamarai regisztrációs szám */
    String mkik

    /* MKKIR SZÁM: */
    String mkkir

    /* VAROS: város */
    String city

    /* UTCA: utca */
    String address1
    String address2

    /* HÁZSZÁM: házszám */
    String postalNumber

    String country

    /* KAPCSOLATTARTÓ */
    String contractPerson

    /* E-MAIL */
    String email

    /* TELEFON */
    String phone

    /* FAX */
    String fax

    /* MOBIL TELEFON */
    String mobile

    /* WEBOLDAL */
    String web

    String extId


    @Override
    public String toString() {
        return "Partner{" +
                "custid='" + custid + '\'' +
                ", name='" + name + '\'' +
                ", compid='" + compid + '\'' +
                ", vatNumber='" + vatNumber + '\'' +
                ", accountNumber1='" + accountNumber1 + '\'' +
                ", accountNumber2='" + accountNumber2 + '\'' +
                ", accountNumber3='" + accountNumber3 + '\'' +
                ", mkik='" + mkik + '\'' +
                ", mkkir='" + mkkir + '\'' +
                ", city='" + city + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", postalNumber='" + postalNumber + '\'' +
                ", country='" + country + '\'' +
                ", contractPerson='" + contractPerson + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", fax='" + fax + '\'' +
                ", mobile='" + mobile + '\'' +
                ", web='" + web + '\'' +
                '}';
    }

    public String toCsv() {
        return "" +
                custid + '; ' +
                extId + '; ' +
                tridokId + '; ' +
                name + '; ' +
                compid + '; ' +
                vatNumber + '; ' +
                accountNumber1 + '; ' +
                accountNumber2 + '; ' +
                accountNumber3 + '; ' +
                mkik + '; ' +
                mkkir + '; ' +
                city + '; ' +
                address1 + '; ' +
                address2 + '; ' +
                postalNumber + '; ' +
                country + '; ' +
                contractPerson + '; ' +
                email + '; ' +
                phone + '; ' +
                fax + '; ' +
                mobile + '; ' +
                web + '\n';
    }

    public static String getHeader() {
        return "" +
                "custid; " +
                "extId; " +
                "tridokId; " +
                "name; " +
                "compid; " +
                "vatNumber; " +
                "accountNumber1; " +
                "accountNumber2; " +
                "accountNumber3; " +
                "mkik; " +
                "mkkir; " +
                "city; " +
                "address1; " +
                "address2; " +
                "postalNumber; " +
                "country; " +
                "contractPerson; " +
                "email; " +
                "phone; " +
                "fax; " +
                "mobile; " +
                "web; " + "\n"
    }

}
