package com.proconx.pdent.tridocmodule.wsclient

import com.proconx.pdent.tridocmodule.*
import com.proconx.pdent.tridocmodule.db.scripts.DownloadPartnerScript
import com.proconx.pdent.tridocmodule.db.scripts.WorknumScript

/**
 * Created by szpetip on 26/11/15.
 */
class TriDokInvoiceParser extends TriDokResponseParser {
    List<Partner> partners

    List<TriDokInvoice> parseInvoices(def src) {
        partners = new DownloadPartnerScript().downloadPartners()

        List<TriDokInvoice> invoices = new ArrayList<>()
        def xml = new XmlParser().parse(src)
        xml.report.item.each { Node item ->
            TriDokInvoice invoice = this.parseInvoice(item)
            invoices.add(invoice)
        }
        return invoices
    }

    Invoice parseInvoice(Node item) {
        TriDokInvoice invoice = new TriDokInvoice()

        String type = getValue(item.regBook.text())
        String worknum = Worknum.normalizeCustId(getValue(item.prjCode.text()))
        String custid = item.regNumName.text()


        item.customField.each { Node customField ->
            def field = parseCustomField(customField)
            setProperty(invoice, field)

        }

        String lastFile = item.lastFile.text()
        if (lastFile.length() > 1 && lastFile.startsWith("0")) {
            invoice.triDokDocId = new Long(lastFile.substring(1))
        }

        String storedPartner = getValue(item.firmName.text())

        if (storedPartner != null && !storedPartner.trim().isEmpty()) {
            invoice.partnerFromDB = true
            invoice.setPartner(storedPartner)
        }

        if (invoice.ownCompany.equals("Aqua General")) {
            invoice.ownCompany = "Aqua - General Kft."
        } else if (invoice.ownCompany.equals("Alföld Európa")) {
            invoice.ownCompany = "Alföld Európa Kft."
        } else if (invoice.ownCompany.equals("Magyar Mélyépítő")) {
            invoice.ownCompany = "Magyar Mélyépítő Kft"
        }

        if (invoice.partner.equals("Hortobágyi Természetvédelmi és Génmegőrz")) {
            invoice.partner = "Hortobágyi Természetvédelmi és Génmegőrző Nonprofit Kft."
        }

        boolean partnerExists = partners.find { Partner p -> p.name.equals(invoice.getPartner()) } != null
        boolean ownCompanyExists = partners.find { Partner p -> p.name.equals(invoice.getOwnCompany()) } != null

        invoice.partnerMissing = !partnerExists
        invoice.ownCompanyMissing = !ownCompanyExists
        invoice.custid = custid
        invoice.type = type == "BE" ? Invoice.CUSTOMER : Invoice.SUPPLIER
        invoice.worknum = getConvertedWorkNum(worknum)
        if (invoice.worknum != null && !invoice.worknum.isEmpty()) {
            boolean wnExists = new WorknumScript().getProjectIdByCustId(invoice.worknum)
            invoice.worknumMissing = !wnExists
        }

        if (invoice.vat == null) invoice.vat = 0.0
        if (invoice.priceInHuf == null) invoice.priceInHuf = 0.0

        invoice.updateInvoice()
        return invoice
    }

    void setProperty(TriDokInvoice invoice, CustomField field) {
        setProperty(invoice, field.key, field.value)
    }

    void setProperty(TriDokInvoice invoice, String key, String value) {
        switch (key) {
            case props.getProperty(CUSTID):
                invoice.custid = value
                break
            case props.getProperty(TYPE):
                invoice.type = value
                break
            case props.getProperty(PARTNER):
                invoice.partner = value
                break
            case props.getProperty(OWN_COMPANY):
                invoice.ownCompany = value
                break
            case props.getProperty(SUPPLIER):
                invoice.supplier = value
                break
            case props.getProperty(CUSTOMER):
                invoice.customer = value
                break
            case props.getProperty(CONTRACT):
                invoice.worknum = value
                break
            case props.getProperty(DATE_OF_COMPLETION):
                invoice.dateOfCompletion = tomorrow(parseDate(value))
                break
            case props.getProperty(DATE_OF_CREATION):
                invoice.dateOfCreation = tomorrow(parseDate(value))
                break
            case props.getProperty(DATE_OF_PAYMENT_DEADLINE):
                invoice.dateOfPaymentDeadline = tomorrow(parseDate(value))
                break
            case props.getProperty(DATE_OF_PAYMENT):
                invoice.dateOfPayment = tomorrow(parseDate(value))
                break
            case props.getProperty(CURRENCY):
                //   println("RRRRRR: ${value} ${value == null}")
                invoice.currency = (value ?: "HUF")
                break
            case props.getProperty(RATE):
                invoice.rate = value != null ? parseNumber(value) : 1
                break
            case props.getProperty(PRICE):
                invoice.price = parseNumber(value)
                break
            case props.getProperty(PRICE_IN_DEV):
                invoice.priceInDev = parseNumber(value)
                break
            case props.getProperty(PRICE_IN_HUF):
                invoice.priceInHuf = parseNumber(value)
                break
            case props.getProperty(VAT):
                if (value == "27%") {
                    invoice.vat = 27
                } else if (value == "18%") {
                    invoice.vat = 18
                } else {
                    invoice.vat = 0
                }
                break
            case props.getProperty(VAT_TYPE):
                invoice.vatType = value != null ? 1 : 0
                break
            case "customField_47":
            case "customField_49":
            case "customField_29":
                //Unused, deprecated worknum property
                break

            default:
                println("Invalid key [$key]: No property existed! Value planned: " + value)
        }
    }

    Date tomorrow(Date date) {
        if (date != null) {
            Calendar calendar = Calendar.getInstance(); // this would default to now
            calendar.setTime(date)
            calendar.add(Calendar.DAY_OF_MONTH, 1)
            return calendar.time
        } else {
            return null
        }
    }

    static TriDokInvoiceParser buildParser(Properties properties) {
        def parser = new TriDokInvoiceParser()
        parser.props = properties
        return parser
    }

    static TriDokInvoiceParser buildDefaultParser() {
        buildParser(defaultProperties())
    }

    public static final String CUSTID = "custid"
    public static final String TYPE = "type"
    public static final String PARTNER = "partner"
    public static final String OWN_COMPANY = "own_company"
    public static final String SUPPLIER = "supplier"
    public static final String CUSTOMER = "customer"
    public static final String CONTRACT = "contract"
    public static final String DATE_OF_COMPLETION = "date_of_completion"
    public static final String DATE_OF_CREATION = "date_of_creation"
    public static final String DATE_OF_PAYMENT_DEADLINE = "date_of_payment_deadline"
    public static final String DATE_OF_PAYMENT = "date_of_payment"
    public static final String CURRENCY = "currency"
    public static final String RATE = "rate"
    public static final String PRICE = "price"
    public static final String PRICE_IN_DEV = "price_in_dev"
    public static final String PRICE_IN_HUF = "price_in_huf"
    public static final String VAT = "vat"
    public static final String VAT_TYPE = "vat_type"


    public static Properties defaultProperties() {
        Properties props = new Properties()
//        props.put(CUSTID, "customField_48")
        props.put(OWN_COMPANY, "customField_16") //OK
        props.put(PARTNER, "customField_28") //OK
        props.put(DATE_OF_PAYMENT_DEADLINE, "customField_34") //OK
        props.put(DATE_OF_COMPLETION, "customField_33") //OK
        props.put(DATE_OF_CREATION, "customField_32") //OK
//        props.put(DATE_OF_PAYMENT, "customField_53")
        props.put(CURRENCY, "customField_31") //OK
//        props.put(RATE, "customField_55")
        props.put(PRICE, "customField_30") //OK
        props.put(VAT, "customField_57")
//        props.put(VAT_TYPE, "customField_58")
        return props
    }


    public static Properties testProperties() {
        Properties props = new Properties()
        props.put(CUSTID, "customField_48")
        props.put(OWN_COMPANY, "customField_16")
        props.put(DATE_OF_PAYMENT_DEADLINE, "customField_50")
        props.put(DATE_OF_COMPLETION, "customField_51")
        props.put(DATE_OF_CREATION, "customField_52")
        props.put(DATE_OF_PAYMENT, "customField_53")
        props.put(CURRENCY, "customField_54")
        props.put(RATE, "customField_55")
        props.put(PRICE, "customField_56")
        props.put(VAT, "customField_57")
        props.put(VAT_TYPE, "customField_58")
        return props
    }
}
