package com.proconx.pdent.tridocmodule.wsclient.merge

import com.proconx.pdent.tridocmodule.Contract
import com.proconx.pdent.tridocmodule.TriDokContract

/**
 * Created by peto.robert on 2015.12.13..
 */
class MergeContractList {
    List<Contract> pde = new ArrayList<>()
    List<TriDokContract> union = new ArrayList<>()

    void merge(List<Contract> pdeContracts, List<TriDokContract> tridokContracts) {
        pdeContracts.each { Contract pdeContract ->
            Boolean paired = false
            tridokContracts.each { TriDokContract tridokContract ->
                if (pdeContract.custid == tridokContract.custid) {
                    pdeContract.copyTo(tridokContract)
                    paired = true
                    union.add(tridokContract)
                }
            }
            if (!paired) {
                pde.add(pdeContract)
            }
        }
    }
}
