package com.proconx.pdent.tridocmodule.wsclient.merge;

import com.proconx.pdent.tridocmodule.MergedPartner;
import com.proconx.pdent.tridocmodule.Partner;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by peto.robert on 2018.04.10..
 */
public class MergePartnerList {
    public List<MergedPartner> union = new ArrayList<>();
    public List<Partner> newInPde = new ArrayList<>();
    public List<Partner> newInTridok = new ArrayList<>();


    public void merge(List<Partner> pdePartners, List<Partner> tridokPartners) {
        pdePartners.stream()
                .forEach(p -> {
                    List<Partner> matchingList = tridokPartners.stream()
                            .filter(t -> p.getExtId() != null &&
                                    (Objects.equals(p.getExtId(), t.getTridokId().toString())))
                            .collect(Collectors.toList());

                    if (matchingList.size() > 1) {
                        System.out.println("More than one match in tridok");
                        matchingList.forEach(System.out::println);
                    } else {
                        if (matchingList.size() == 1) {
                            matchingList.get(0).setPdeId(p.getPdeId());
                            MergedPartner mp = new MergedPartner(p, matchingList.get(0));
                            union.add(mp);
                        } else {
                            newInPde.add(p);
                        }
                    }
                });

        tridokPartners.stream()
                .forEach(t -> {
                    List<Partner> matchingList = pdePartners.stream()
                            .filter(p ->
                                    (Objects.equals(p.getExtId(), t.getTridokId().toString()))
                            )
                            .peek(x -> {
                                t.setPdeId(x.getPdeId());
                            })
                            .collect(Collectors.toList());

                    if (matchingList.size() > 1) {
                        System.out.println("More than one match in pde");
                        matchingList.forEach(System.out::println);
                    } else {
                        if (matchingList.size() == 1) {

                            MergedPartner mp = new MergedPartner(t, matchingList.get(0));
                            union.add(mp);
                        } else {
                            t.setCustid("T" + (t.getTridokId() + 1000));
                            t.setExtId(t.getTridokId().toString());
                            newInTridok.add(t);
                        }
                    }
                });


        System.out.println();

    }

    ;


}
