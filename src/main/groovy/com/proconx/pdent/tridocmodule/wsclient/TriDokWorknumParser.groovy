package com.proconx.pdent.tridocmodule.wsclient

import com.proconx.pdent.tridocmodule.Worknum

/**
 * Created by peto.robert on 2016.01.22..
 */
class TriDokWorknumParser extends TriDokResponseParser {

    List<Worknum> parseWorkNums(def src) {

        List<Worknum> worknums = new ArrayList<>()
        def xml = new XmlParser().parse(src)

        xml.lov.item.each { Node item ->
            Worknum worknum = this.parseWorkNum(item)
            worknums.add(worknum)
        }
        return worknums
    }

    Worknum parseWorkNum(Node item) {
        Long id = Long.parseLong(item.id.text())
        String custid = getValue(item.name.text())
        Worknum worknum = new Worknum(id: id, custid: custid)
        return worknum
    }

    static TriDokWorknumParser buildDefaultParser() {
        return new TriDokWorknumParser()
    }

}
