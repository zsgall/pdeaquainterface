package com.proconx.pdent.tridocmodule.wsclient.merge

import com.proconx.pdent.tridocmodule.Invoice
import com.proconx.pdent.tridocmodule.TriDokInvoice

/**
 * Created by peto.robert on 2015.12.15..
 */
class MergeInvoiceList {


    List<TriDokInvoice> tridokInvoices = new ArrayList<>()
    List<Invoice> union = new ArrayList<>()

    void merge(List<Invoice> pdeInvoices, List<Invoice> invoices) {
        invoices.each { Invoice tridokInvoice ->
            def matched = false
            pdeInvoices.each { Invoice pdeInvoice ->
//                println "I: ${tridokInvoice.custid}-${tridokInvoice.worknum} P: ${pdeInvoice.custid}-${pdeInvoice.worknum}"
                if (tridokInvoice.custid == pdeInvoice.custid && tridokInvoice.worknum == pdeInvoice.worknum) {
                    matched = true
//                    println "MMMMMMMM: ${tridokInvoice.toString()}"
                    union.add(tridokInvoice)
                }
            }
            if (!matched) {
                tridokInvoices.add(tridokInvoice)
            }
        }
    }
}
