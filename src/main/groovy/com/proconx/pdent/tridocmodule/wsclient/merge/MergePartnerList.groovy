package com.proconx.pdent.tridocmodule.wsclient.merge

import com.proconx.pdent.tridocmodule.MergedPartner
import com.proconx.pdent.tridocmodule.Partner


/**
 * Created by peto.robert on 2015.12.12..
 */
class MergePartnerListG {
    List<MergedPartner> union = new ArrayList<>()
    List<Partner> newInPde = new ArrayList<>()
    List<Partner> newInTridok = new ArrayList<>()

    void merge(List<Partner> pdePartners, List<Partner> tridokPartners) {
        pdePartners.each { Partner pdePartner ->
            Boolean paired = false
            tridokPartners.each { Partner tridokPartner ->
                String tricustid = "T${(1000 + tridokPartner.tridokId)}"

                if (Objects.equals(pdePartner.vatNumber, tridokPartner.vatNumber) || Objects.equals(pdePartner.compid, tridokPartner.compid)) {
                    tridokPartner.pdeId = pdePartner.pdeId
                    MergedPartner mp = new MergedPartner(pdePartner, tridokPartner)
                    union.add(mp)
                    paired = true
                }
            }

            if (!paired) {
                newInPde.add(pdePartner)
            }
        }
        tridokPartners.each { Partner tridokPartner ->
            Boolean paired = false
            pdePartners.each { Partner pdePartner ->

                if (Objects.equals(pdePartner.vatNumber, tridokPartner.vatNumber) || Objects.equals(pdePartner.compid, tridokPartner.compid)) {
                    paired = true
                }
            }

            if (!paired) {
                tridokPartner.custid = "T${(1000 + tridokPartner.tridokId)}"
                newInTridok.add(tridokPartner)
            }
        }
    }
}
