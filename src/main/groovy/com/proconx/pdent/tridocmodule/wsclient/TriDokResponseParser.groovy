package com.proconx.pdent.tridocmodule.wsclient

import com.proconx.pdent.tridocmodule.CustomField

import java.text.SimpleDateFormat

/**
 * Created by szpetip on 26/11/15.
 */
class TriDokResponseParser {


    Properties props
    Properties worknumProperties

    String getConvertedWorkNum(String worknum) {
        return worknumProperties != null ? worknumProperties.getProperty(worknum, worknum) : worknum
    }

    CustomField parseCustomField(Node customField) {
        String key = getValue(customField.name.text())
        String value = getValue(customField.value.text())
        return new CustomField(key: key, value: value)
    }


    Date parseDate(String str) {
        // Sample: 2015-11-29T23:00:00Z
        if (str.length() >= 10) {
            String d = str.substring(0, 10)
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd")
            return sdf.parse(d)
        }
        return null
    }

    Double parseNumber(String str) {
        try {
            return new Double(str)
        } catch (Exception ex) {
            return null
        }
    }


    String getValue(String cData) {
        def data = cData.trim();
        if (data.startsWith("<![CDATA[") && data.endsWith("]]>")) {
            return data.substring(9, data.length() - 3)
        } else {
            return data;
        }
    }

    String setValue(String cData) {
        return "<![CDATA[$cData]]>"
    }
}
