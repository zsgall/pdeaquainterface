package com.proconx.pdent.tridocmodule.wsclient

import com.proconx.pdent.tridocmodule.Partner


/**
 * Created by peto.robert on 2015.12.12..
 */
class TriDokPartnerParser extends TriDokResponseParser {

    List<Partner> parsePartners(def src) {
        List<Partner> partners = new ArrayList<>()
        def xml = new XmlParser().parse(src)
        xml.report.item.each { Node item ->
            Partner partner = this.parsePartner(item)
            partners.add(partner)
        }
        return partners
    }

    Partner parsePartner(Node item) {
        Partner partner = new Partner()
        partner.tridokId = getValue(item.id.text()).toLong()
        partner.city = getValue(item.city.text())
        partner.address1 = getValue(item.street.text())
        partner.postalNumber = getValue(item.zipCode.text())
        partner.name = getValue(item.longName.text())
        partner.vatNumber = getValue(item.taxRegNum.text())
        partner.phone = getValue(item.phone.text())
        partner.fax = getValue(item.fax.text())
        partner.accountNumber1 = getValue(item.accountNumber1.text())
        partner.accountNumber2 = getValue(item.accountNumber2.text())
        partner.email = getValue(item.email.text())

        item.customField.each { Node customField ->
            def field = parseCustomField(customField)
            if (field.key == "customField_12") {
                partner.mkik = field.value
            } else if (field.key == "customField_13") {
                partner.mkkir = field.value
            }
        }
        return partner
    }


    String generate(Partner partner) {

        def ownerUserer = """<tripartner:createUser  xmlns:tridoc="http://www.trilobita.hu/schema/tridoc/model-1_0_0">
                <triadmin:userName xmlns:triadmin="http://www.trilobita.hu/schema/triadmin/model-1_0_0">ProCon</triadmin:userName>
       </tripartner:createUser>"""




        String mkik = partner.mkik == null || partner.mkik?.equals("") ? "" : """<tripartner:customField>
                 <triadmin:name xmlns:triadmin="http://www.trilobita.hu/schema/triadmin/model-1_0_0"><![CDATA[customField_12]]></triadmin:name>
                <triadmin:type xmlns:triadmin="http://www.trilobita.hu/schema/triadmin/model-1_0_0"><![CDATA[dataTypeTextInput]]></triadmin:type>

        <triadmin:value xmlns:triadmin="http://www.trilobita.hu/schema/triadmin/model-1_0_0"><![CDATA[${partner.mkik}]]></triadmin:value>
         <triadmin:multiCustomField xmlns:triadmin="http://www.trilobita.hu/schema/triadmin/model-1_0_0">
          <triadmin:id>12</triadmin:id>
        </triadmin:multiCustomField>
      </tripartner:customField>
"""

        String mkkir = partner.mkkir == null || partner.mkkir?.equals("") ? "" : """<tripartner:customField>
                 <triadmin:name xmlns:triadmin="http://www.trilobita.hu/schema/triadmin/model-1_0_0"><![CDATA[customField_13]]></triadmin:name>
                <triadmin:type xmlns:triadmin="http://www.trilobita.hu/schema/triadmin/model-1_0_0"><![CDATA[dataTypeTextInput]]></triadmin:type>

        <triadmin:value xmlns:triadmin="http://www.trilobita.hu/schema/triadmin/model-1_0_0"><![CDATA[${partner.mkkir}]]></triadmin:value>
         <triadmin:multiCustomField xmlns:triadmin="http://www.trilobita.hu/schema/triadmin/model-1_0_0">
          <triadmin:id>13</triadmin:id>
        </triadmin:multiCustomField>
      </tripartner:customField>
      """

        """
<request>
\t<action>save</action>
  <entity>
    <tripartner:firm xmlns:tripartner="http://www.trilobita.hu/schema/tripartner/model-1_0_0" >
        ${partner.tridokId != null ? "<tripartner:id>${partner.tridokId}</tripartner:id>" : ""}
<tripartner:shortName><![CDATA[${partner.name}]]></tripartner:shortName>
<tripartner:longName><![CDATA[${partner.name}]]></tripartner:longName>

      <tripartner:firmType>
       <tripartner:id>1</tripartner:id>
            </tripartner:firmType>
       <tripartner:firmStatus>
       <tripartner:id>1</tripartner:id>
            </tripartner:firmStatus>
      ${
            !partner.postalNumber?.equals("") ? "<tripartner:seatZipCode><![CDATA[${partner.postalNumber}]]></tripartner:seatZipCode>" : ""
        }
      ${
            partner.city != null && !partner.city?.equals("") ? "<tripartner:seatCityName><![CDATA[${partner.city}]]></tripartner:seatCityName>" : ""
        }
      ${
            partner.address1 != null && !partner.address1?.equals("") ? "<tripartner:seatStreet><![CDATA[${partner.address1}${partner.address2 != null && partner.address2 != "" ? " ${partner.address2}" : ""}]]></tripartner:seatStreet>" : ""
        }
      ${
            partner.phone != null && !partner.phone?.equals("") ? "<tripartner:phone>${partner.phone}</tripartner:phone>" : ""
        }
      ${partner.fax != null && !partner.fax?.equals("") ? "<tripartner:fax>${partner.fax}</tripartner:fax>" : ""}
      ${
            partner.email != null && !partner.email?.equals("") ? "<tripartner:email>${partner.email}</tripartner:email>" : ""
        }
      ${partner.web != null && !partner.web?.equals("") ? "<tripartner:web>${partner.web}</tripartner:web>" : ""}
      ${partner.tridokId != null ? ownerUserer : ""}
      $mkik
      $mkkir

    </tripartner:firm>
  </entity>
</request>

"""
    }

    static TriDokPartnerParser buildDefaultParser() {
        return new TriDokPartnerParser()
    }
}
