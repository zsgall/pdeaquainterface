package com.proconx.pdent.tridocmodule.wsclient

import com.proconx.pdent.tridocmodule.*

import java.text.SimpleDateFormat

/**
 * Created by szpetip on 02/12/15.
 */
class TriDokContractParser extends TriDokResponseParser {
    public static final int PDE_CONTRACT_TYPE = 193;
    public static final int PDE_CONTRACT_CATEGORY = 157;

    List<Partner> partners

    List<TriDokContract> parseContractChunks(def src) {
        List<TriDokContract> invoices = new ArrayList<>()
        def xml = new XmlParser().parse(src)
        xml.report.item.each { Node item ->
            TriDokContract contract = this.parseContract(item)
            invoices.add(contract)
        }
        return invoices
    }

    TriDokContract parseContract(Node item) {
        //  println "I: ${item}"
        TriDokContract contract = new TriDokContract()
        Long id = item.id.text().toLong()
        String custid = getValue(item.shortTitle.text())
        String name = getValue(item.longTitle.text())
        String iktatoszam = getValue(item.regNumName.text())

        contract.contractType = com.proconx.pdent.tridocmodule.job.tridok.ContractType.getBy(Long.parseLong(item.docCategory.text()), Long.parseLong(item.docTypeId.text()))

        contract.tridokId = id
        contract.custid = custid
        contract.name = name
        contract.iktatoszam = iktatoszam
        contract.worknum = Worknum.normalizeCustId(getValue(item.prjCode.text()))
        return contract
    }


    String generate(Contract contract) {
        Project project = Project.projectByPdeName(contract.worknum)
        if (project == null) {
            System.err.println("Project not found: ${contract.worknum}")
//            throw new Exception("Project not found!")
        }

        String id = null
        String iktatoszam = null;
        if (contract instanceof TriDokContract) {
            def tridokContract = contract
            if (tridokContract.tridokId != null && tridokContract.tridokId > 0) {
                id = tridokContract.tridokId
                iktatoszam = tridokContract.iktatoszam
            }
        }



        def ownerUser = """ <tridoc:owner><triadmin:userName xmlns:triadmin="http://www.trilobita.hu/schema/triadmin/model-1_0_0"><![CDATA[ProCon]]></triadmin:userName></tridoc:owner>"""

        def createUser = """
      <tridoc:createUser>
        <triadmin:userName xmlns:triadmin="http://www.trilobita.hu/schema/triadmin/model-1_0_0">ProCon</triadmin:userName>
      </tridoc:createUser>
      <tridoc:modifyUser>
        <triadmin:userName xmlns:triadmin="http://www.trilobita.hu/schema/triadmin/model-1_0_0">
            <![CDATA[ProCon]]>
        </triadmin:userName>
      </tridoc:modifyUser>
 """
        def projectXml = project != null ? """        <tridoc:prj>
            <tridoc:id>${project.tridokId}</tridoc:id>
        </tridoc:prj>
""" : ""

        def regNumName = "<tridoc:regNumName><![CDATA[${iktatoszam}]]></tridoc:regNumName>"
        def customFields = toXml(contract)
        return """
<request>
<action>save</action>
  <entity>
    <tridoc:doc xmlns:tridoc="http://www.trilobita.hu/schema/tridoc/model-1_0_0">
        ${id != null ? "<tridoc:id>$id</tridoc:id>" : ""}
      <tridoc:shortTitle><![CDATA[${contract.custid}]]></tridoc:shortTitle>
        <tridoc:longTitle><![CDATA[${contract.name}]]></tridoc:longTitle>
        <tridoc:docType>
            <tridoc:id>${contract.contractType.type}</tridoc:id>
        </tridoc:docType>
        <tridoc:docCategory>
            <tridoc:id>${contract.contractType.category}</tridoc:id>
        </tridoc:docCategory>
        ${id != null ? ownerUser : ""}
        <tridoc:docStoreInterval>
            <tridoc:id>2</tridoc:id>
        </tridoc:docStoreInterval>
        <tridoc:regBook>
            <tridoc:id>${contract.type == Contract.TYPE_ALVALLALKOZOI ? "2" : "4"}</tridoc:id>
        </tridoc:regBook>
         ${id != null ? regNumName : ""}
        $projectXml
        <tridoc:locked>false</tridoc:locked>
        <tridoc:active>1</tridoc:active>
        ${id != null ? createUser : ""}
     ${customFields}
     </tridoc:doc>
  </entity>
</request>
    """
    }

    String parseDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd")
        return "${sdf.format(date)}T00:00:00Z"
    }


    String toXml(Contract contract) {
        StringBuilder sb = new StringBuilder()

        if (contract.contractor == "Aqua - General Kft.") {
            contract.contractor = "Aqua General"
//            contract.tridokCategory = "28"
//            contract.tridokType = "53"

        } else if (contract.contractor == "Alföld Európa Kft.") {
            contract.contractor = "Alföld Európa"
//            contract.tridokCategory = "29"
//            contract.tridokType = "54"
        } else if (contract.contractor == "Magyar Mélyépítő Kft") {
            contract.contractor = "Magyar Mélyépítő"
//            contract.tridokCategory = "30"
//            contract.tridokType = "56"
        }

//        if (contract.type == Contract.TYPE_ALVALLALKOZOI) {
        String partnerId = null

        partners?.each { Partner partner ->
            // println "XP: ${partner.name} ${partner.tridokId} ${contract.partner}"
            if (partner.name == contract.partner) {
                //  println "HOOO: ${partner.name} = ${contract.name} = ${partner.tridokId.toString()}"
                partnerId = partner.tridokId.toString()
            }
        }
        //    println "XPRESULT: ${partnerId}"
        sb.append("<tridoc:firm><tripartner:id xmlns:tripartner=\"http://www.trilobita.hu/schema/tripartner/model-1_0_0\">${partnerId}</tripartner:id></tridoc:firm>")
//            sb.append(addCustomField(TYPE, Contract.TYPE_ALVALLALKOZOI))
        sb.append(addCustomField(OWNCOMPANY, contract.contractor))
//        } else {
//            String partnerId = 434
//            sb.append("<tridoc:firm><![CDATA[$partnerId]]></tridoc:firm>")
//            sb.append(addCustomField(TYPE, Contract.TYPE_MEGRENDELOI))
//            sb.append(addCustomField(CONTRACTOR, contract.partner))
//        }

        sb.append(addCustomField(NAME, contract.name))

        if (contract.dateOfContract != null) {
            sb.append(addCustomField(DATE_OF_CONTRACT, parseDate(contract.dateOfContract)))
        }
        if (contract.dateOfStart != null) {
            sb.append(addCustomField(DATE_OF_START, parseDate(contract.dateOfStart)))
        }
        if (contract.dateOfFinish != null) {
            sb.append(addCustomField(DATE_OF_FINISH, parseDate(contract.dateOfFinish)))
        }

        if (contract.price != null) {
            sb.append(addCustomField(PRICE, contract.price.toInteger().toString()))
            sb.append(addCustomField(CURRENCY, "HUF"))
        }

        if (contract.vatType == "1") {
            sb.append(addCustomField(VAT, "FA"))
        } else {
            sb.append(addCustomField(VAT, (contract.vat == 27D ? "27%" : "0%")))
        }

//        if (contract.vatType == "1") {
//            sb.append(addCustomField(VAT_TYPE, "true"))
//        }

//        if (contract.dayOfPaymentDeadline != null) {
//            sb.append(addCustomField(DATE_OF_PAYMENT_DEADLINE, contract.dayOfPaymentDeadline.toString()))
//        }

        return sb.toString()
        /*
    public static final String ADVANCED_VALUE = "advancedValue"
    public static final String DATE_OF_ADVANCED_VALUE_PAYMENT = "dateOfAdvancedValuePayment"
    public static final String RET_PERC = "retPerc"
    public static final String DATE_OF_RET_PERC = "dateOfRetPerc"
    public static final String WAR_PERC = "warPerc"
    public static final String DEADLINE_OF_WAR = "deadlineOfWar"
    public static final String DEADLINE_OF_WAR_UNIT = "deadlineOfWarUnit"
    public static final String SERVICE_VALUE = "serviceValue"
         */


    }

    String addProperty(String key, String value) {
        return "<tridoc:${props.getProperty(key)}>${value}</tridoc:${props.getProperty(key)}>"
    }

    String addCustomField(String key, String value) {
        if (value == null || value.isEmpty()) {
            return ""
        } else {
            String code = props.getProperty(key)
            String tridokId = "customField_$code"
            String tridokType = getTridokType(key)
            if (code == null || code.isEmpty()) {
                return ""
            } else {
                return """
      <tridoc:customField>
                 <triadmin:name xmlns:triadmin="http://www.trilobita.hu/schema/triadmin/model-1_0_0"><![CDATA[$tridokId]]></triadmin:name>
                <triadmin:type xmlns:triadmin="http://www.trilobita.hu/schema/triadmin/model-1_0_0"><![CDATA[$tridokType]]></triadmin:type>

        <triadmin:value xmlns:triadmin="http://www.trilobita.hu/schema/triadmin/model-1_0_0"><![CDATA[$value]]></triadmin:value>
         <triadmin:multiCustomField xmlns:triadmin="http://www.trilobita.hu/schema/triadmin/model-1_0_0">
          <triadmin:id>$code</triadmin:id>
        </triadmin:multiCustomField>
      </tridoc:customField>
"""
            }
        }
    }

    void setProperty(TriDokContract contract, CustomField field) {
        setProperty(contract, field.key, field.value)
    }

    void setProperty(TriDokContract contract, String key, String value) {
        switch (key) {
            case props.getProperty(CUSTID):
                contract.custid = value
                break
            case props.getProperty(TYPE):
                contract.type = value
                break
            case props.getProperty(NAME):
                contract.name = value
                break
            case props.getProperty(WORKNUM):
                contract.worknum = value
                break
            case props.getProperty(PARTNER):
                contract.partner = value
                break
            case props.getProperty(OWNCOMPANY):
                contract.contractor = value
                break
            case props.getProperty(DATE_OF_CONTRACT):
                contract.dateOfContract = parseDate(value)
                break
            case props.getProperty(DATE_OF_START):
                contract.dateOfStart = parseDate(value)
                break
            case props.getProperty(DATE_OF_FINISH):
                contract.dateOfFinish = parseDate(value)
                break
            case props.getProperty(PRICE):
                contract.price = parseNumber(value)
                break
            case props.getProperty(CURRENCY):
                contract.currency = value
                break
            case props.getProperty(VAT):
                contract.vat = parseNumber(value)
                break
//            case props.getProperty(VAT_TYPE):
//                contract.vat = parseNumber(value)
//                break
//            case props.getProperty(DATE_OF_PAYMENT_DEADLINE):
//                contract.dateOfPaymentDeadline = parseDate(value)
//                break
//            case props.getProperty(ADVANCED_VALUE):
//                contract.advancedValue = parseNumber(value)
//                break
//            case props.getProperty(DATE_OF_ADVANCED_VALUE_PAYMENT):
//                contract.dateOfAdvancedValuePayment = parseDate(value)
//                break
//            case props.getProperty(RET_PERC):
//                contract.retPerc = parseNumber(value)
//                break
//            case props.getProperty(DATE_OF_RET_PERC):
//                contract.dateOfRetPerc = parseDate(value)
//                break
//            case props.getProperty(WAR_PERC):
//                contract.warPerc = parseNumber(value)
//                break
//            case props.getProperty(DEADLINE_OF_WAR):
//                contract.deadlineOfWar = parseNumber(value)
//                break
//            case props.getProperty(DEADLINE_OF_WAR_UNIT):
//                contract.deadlineOfWarUnit = value
//                break
//            case props.getProperty(SERVICE_VALUE):
//                contract.serviceValue = parseNumber(value)
//                break
        }
    }

    public static final String CUSTID = "custid"
    public static final String TYPE = "type"
    public static final String NAME = "name"
    public static final String WORKNUM = "worknum"
    public static final String PARTNER = "firm"
    public static final String OWNCOMPANY = "ownCompany"
    public static final String DATE_OF_CONTRACT = "dateOfContract"
    public static final String DATE_OF_START = "dateOfStart"
    public static final String DATE_OF_FINISH = "dateOfFinish"
    public static final String PRICE = "price"
    public static final String CURRENCY = "currency"

    public static final String VAT = "vat"
//    public static final String VAT_TYPE = "vatType"
//    public static final String DATE_OF_PAYMENT_DEADLINE = "dateOfPaymentDeadline"
//    public static final String ADVANCED_VALUE = "advancedValue"
//    public static final String DATE_OF_ADVANCED_VALUE_PAYMENT = "dateOfAdvancedValuePayment"
//    public static final String RET_PERC = "retPerc"
//    public static final String DATE_OF_RET_PERC = "dateOfRetPerc"
//    public static final String WAR_PERC = "warPerc"
//    public static final String DEADLINE_OF_WAR = "deadlineOfWar"
//    public static final String DEADLINE_OF_WAR_UNIT = "deadlineOfWarUnit"
//    public static final String SERVICE_VALUE = "serviceValue"

    static TriDokContractParser buildParser(Properties properties) {
        def parser = new TriDokContractParser()
        parser.props = properties
        return parser
    }


    static TriDokContractParser buildDefaultParser() {
        buildParser(defaultProperties())
    }

    ContractType getType(Long o, Long t) {
        return new ContractType(category: 28, type: 53)
    }

    class ContractType {
        Long category
        Long type

    }


    public static String getTridokType(String key) {
        switch (key) {
//            case TYPE:
            case OWNCOMPANY:
            case CURRENCY:
                return "dataTypeList"
            case DATE_OF_CONTRACT:
            case DATE_OF_FINISH:
            case DATE_OF_START:
                return "dataTypeDate"
//            case DATE_OF_PAYMENT_DEADLINE:
            case PRICE:
                return "dataTypeLong"
//            case VAT_TYPE:
//                return "dataTypeBoolean"
            default:
                return "dataTypeTextInput"
        }
    }

    public static Properties defaultProperties() {
        Properties props = new Properties()
        props.put(DATE_OF_CONTRACT, "35") //OK
        props.put(NAME, "36") //OK
        props.put(PRICE, "37") //OK
        props.put(CURRENCY, "38") //OK
        props.put(DATE_OF_START, "39") //OK
        props.put(DATE_OF_FINISH, "40") //OK
        props.put(VAT, "46") //OK
        props.put(OWNCOMPANY, "16") //OK

//
//        props.put(VAT_TYPE, "67")
//        props.put(DATE_OF_PAYMENT_DEADLINE, "68")
//        props.put(ADVANCED_VALUE, "58")
//        props.put(DATE_OF_ADVANCED_VALUE_PAYMENT, "58")
//        props.put(RET_PERC, "58")
//        props.put(DATE_OF_RET_PERC, "58")
//        props.put(WAR_PERC, "58")
//        props.put(DEADLINE_OF_WAR, "58")
//        props.put(DEADLINE_OF_WAR_UNIT, "58")
//        props.put(SERVICE_VALUE, "58")
//        props.put(TYPE, "59")
//        props.put(CONTRACTOR, "16")
        return props
    }
}
