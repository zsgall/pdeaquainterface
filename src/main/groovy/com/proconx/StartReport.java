package com.proconx;

import com.proconx.pdent.tridocmodule.job.full.ExecuteInvoiceJob;
import com.proconx.pdent.tridocmodule.job.full.ExecutePartnerJob;

public class StartReport {

    public static void main(String[] args) {
        if  (args.length==1) {
            switch (args[0]) {
                case "Report":
                    new ExecutePartnerJob().executeReport();
                    new ExecuteInvoiceJob().executeReport();
                    //new ExecuteContractJob().executeReport()
                    break;
                case "Upload":
                    new ExecutePartnerJob().execute();
                    new ExecuteInvoiceJob().execute();
                    //new ExecuteContractJob().execute()
                    break;
                default:
                    System.out.println("Unknown command. Available commands are Report, Upload");
            }
        } else {
            System.out.println("Bad arguments. Available commands are Report, Upload");
        }
    }

}
