package com.proconx.pdent.tridocmodule.test

import com.proconx.pdent.tridocmodule.Partner
import com.proconx.pdent.tridocmodule.wsclient.TriDokPartnerParser

/**
 * Created by peto.robert on 2016.02.16..
 */
class TriDokPartnerUpdateTest {

    static void testTriDokUpdate() {
        Partner partner = new Partner(
                tridokId: 592,
                name: "PDE Moka Miki Sr.",
                postalNumber: "1111",
                city: "Budapest",
                address1: "Legjobb utca 1",
                phone: "+36-20-12345678",
                mkkir: "MKKIR-02",
                mkik: "MKIK/002",
                email: "miki@moka.hu",
                web: "moka.hu"
        )
        TriDokPartnerParser parser = TriDokPartnerParser.buildDefaultParser()
        def xml = parser.generate(partner)
        println xml
    }

    public static void main(String[] args) {
        testTriDokUpdate()
    }
}
